import React from "react";
import { Query } from "react-apollo";
import { query } from "./GraphQL";
import { Spin } from "antd";

const Session = (Component) => (props) =>
  (
    <Query query={query.USUARIO_ACTUAL}>
      {({ loading, error, data, refetch }) => {
        if (loading)
          return (
            <div
              style={{
                width: "100%",
                height: "100vh",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <Spin />

              <p style={{ marginLeft: 10, marginTop: 5 }}>Loading ....</p>
            </div>
          );
        if (error) return console.log(error);
        return <Component {...props} refetch={refetch} session={data} />;
      }}
    </Query>
  );

export default Session;
