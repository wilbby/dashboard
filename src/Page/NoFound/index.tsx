import React from "react";
import { Result, Button } from "antd";

export default function NoFound() {
  return (
    <div>
      <Result
        status="404"
        title="404 Página no encontrada"
        subTitle="Lo sentimos, la página que visitaste no existe."
        extra={
          <Button type="primary" href="/" shape="round">
            Volver al inicio
          </Button>
        }
      />
    </div>
  );
}
