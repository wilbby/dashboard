import gql from "graphql-tag";

const USER_DETAIL = gql`
  query getUsuarioAdmin {
    getUsuarioAdmin {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        telefono
        city
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
      }
    }
  }
`;

const USUARIO_ACTUAL = gql`
  query obtenerAdmin {
    obtenerAdmin {
      _id
      name
      lastName
      email
      avatar
      telefono
      city
      created_at
      updated_at
      termAndConditions
      verifyPhone
      StripeID
      OnesignalID
    }
  }
`;

const GET_ORDER = gql`
  query getNewOrderAdimin(
    $status: [String]
    $dateRange: DateRangeInput
    $page: Int
    $limit: Int
    $orderID: Int
    $Stores: [String]
    $city: String
    $rider: [String]
  ) {
    getNewOrderAdimin(
      status: $status
      dateRange: $dateRange
      page: $page
      limit: $limit
      orderID: $orderID
      Stores: $Stores
      city: $city
      rider: $rider
    ) {
      message
      success
      count
      list {
        _id
        channelOrderDisplayId
        orderType
        pickupTime
        estimatedPickupTime
        deliveryTime
        courier
        tip
        scheduled
        cupon
        created_at
        cuponName
        cuponTipe
        cuponValue
        source
        pagoPaypal
        stripePaymentIntent
        courierData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
          rating
        }
        customerData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
        }
        customer
        store
        storeData
        deliveryAddressData {
          _id
          formatted_address
          puertaPiso
          city
          postalcode
          type
          usuario
          lat
          lgn
        }
        deliveryAddress
        orderIsAlreadyPaid
        payment
        note
        items
        numberOfCustomers
        deliveryCost
        serviceCharge
        discountTotal
        IntegerValue
        paymentMethod
        statusProcess
        Needcutlery
        status
        invoiceUrl
      }
    }
  }
`;

const GET_RIDER = gql`
  query getRiderForAdmin($city: String) {
    getRiderForAdmin(city: $city) {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        city
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
        isAvalible
        rating
        numberCurrentOrder
      }
    }
  }
`;

const GET_RIDER_ALL = gql`
  query getRiderForAdminAll {
    getRiderForAdminAll {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        city
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
        isAvalible
        rating
        numberCurrentOrder
      }
    }
  }
`;

const GET_CUSTOM_ORDER = gql`
  query getCustomOrderAdmin(
    $city: String
    $orderID: Int
    $dateRange: DateRangeInput
    $limit: Int
    $page: Int
  ) {
    getCustomOrderAdmin(
      city: $city
      orderID: $orderID
      dateRange: $dateRange
      limit: $limit
      page: $page
    ) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          rating
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        customer
        fronStore
        storeID
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
        pagoPaypal
        stripePaymentIntent
        customer
        fronStore
        storeID
        Store {
          _id
          title
          image
          city
          phone
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
        }
      }
    }
  }
`;

const CUSTOMERS = gql`
  query getCustomers($page: Int, $limit: Int, $email: String) {
    getCustomers(page: $page, limit: $limit, email: $email) {
      success
      messages
      count
      data {
        _id
        name
        lastName
        email
        city
        avatar
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        isAvalible
        StripeID
        PaypalID
        OnesignalID
        socialNetworks
        isSocial
      }
    }
  }
`;

const ALL_CUSTOMERS = gql`
  query getAllCustomers($email: String) {
    getAllCustomers(email: $email) {
      success
      messages
      data {
        _id
        name
        lastName
        email
        city
        avatar
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        isAvalible
        StripeID
        PaypalID
        OnesignalID
        socialNetworks
        isSocial
      }
    }
  }
`;

const RESTAURANT = gql`
  query getRestaurant($city: String) {
    getRestaurant(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        type
        salvingPack
        noScheduled
        forttalezaURL
        openURL
        url
        aceptReservation
        reservationScheduled
        stripeID
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        stimateTime
        ispartners
        highkitchen
        shipping
        previous_shipping
        stimateTime
        highkitchen
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        ispartners
        inTop
        sort
        inHouse
        adsInHouse
        showAlergeno
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const RESTAURANT_EDIT = gql`
  query getRestaurantforEdit(
    $city: String
    $search: String
    $page: Int
    $limit: Int
  ) {
    getRestaurantforEdit(
      city: $city
      search: $search
      page: $page
      limit: $limit
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        slug
        inOffert
        stripeID
        type
        salvingPack
        forttalezaURL
        openURL
        url
        aceptReservation
        reservationScheduled
        noScheduled
        inTop
        sort
        inHouse
        adsInHouse
        showAlergeno
        socialLink {
          instagram
          facebook
          twitter
          web
        }
        scheduleOnly {
          available
          hour
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        city
        categoryID
        autoshipping
        collections
        isDeliverectPartner
        alegeno_url
        categoryName
        minime
        stimateTime
        ispartners
        highkitchen
        shipping
        previous_shipping
        stimateTime
        highkitchen
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        ispartners
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

const CATEGORY = gql`
  query getCategory($city: String) {
    getCategory(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        description
        sorting
        excludeCity
        visible
        navigate
        url
      }
    }
  }
`;

const TIPO = gql`
  query getTipo {
    getTipo {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const HighkitchenCategory = gql`
  query getHighkitchenCategory {
    getHighkitchenCategory {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const TIPO_TIENDAS = gql`
  query getTipoTienda {
    getTipoTienda {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

const GET_RANDOM_ADS = gql`
  query getAds($city: String) {
    getAds(city: $city) {
      messages
      success
      data {
        _id
        name
        image
        sorting
        visible
        navigate
        includeCity
        url
        email
        category
        isHome
        dataEmail
        click
        created_at
        end_date
      }
    }
  }
`;

const GET_RESERVAS = gql`
  query getAllReservaAdmin($page: Int, $limit: Int) {
    getAllReservaAdmin(page: $page, limit: $limit) {
      success
      messages
      count
      data {
        _id
        store
        user
        date
        hour
        people
        note
        city
        status
        userID
        storeID
        created_at
        updated_at
      }
    }
  }
`;

const GET_SUBCOLLECTION = gql`
  query getSubCollectionAdmin {
    getSubCollectionAdmin {
      success
      message
      data {
        _id
        title
      }
    }
  }
`;


export const query = {
  USER_DETAIL,
  USUARIO_ACTUAL,
  GET_ORDER,
  GET_RIDER,
  GET_RIDER_ALL,
  GET_CUSTOM_ORDER,
  CUSTOMERS,
  ALL_CUSTOMERS,
  RESTAURANT,
  RESTAURANT_EDIT,
  CATEGORY,
  TIPO,
  HighkitchenCategory,
  TIPO_TIENDAS,
  GET_RANDOM_ADS,
  GET_RESERVAS,
  GET_SUBCOLLECTION
};
