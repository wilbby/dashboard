import { mutation } from "./mutation";
import { query } from "./query";

export { query, mutation };
