import gql from "graphql-tag";

const AUTENTICAR_ADMIN = gql`
  mutation autenticarAdmin($email: String!, $password: String!) {
    autenticarAdmin(email: $email, password: $password) {
      success
      message
      data {
        token
        id
      }
    }
  }
`;

const NUEVO_ADMIN = gql`
  mutation crearAdmin($input: UsuarioInput) {
    crearAdmin(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
      }
    }
  }
`;

const ACTUALIZAR_ADMIN = gql`
  mutation actualizarAdmin($input: ActualizarUsuarioInput) {
    actualizarAdmin(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: String) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

const ELIMINAR_ADMIN = gql`
  mutation eliminarAdmin($id: ID!) {
    eliminarAdmin(id: $id) {
      success
      messages
    }
  }
`;

const CREATE_NEW_ORDER = gql`
  mutation crearModificarNewOrden($input: NewOrderInput) {
    crearModificarNewOrden(input: $input) {
      success
      message
      data {
        _id
        channelOrderDisplayId
        orderType
        pickupTime
        estimatedPickupTime
        deliveryTime
        courier
        courierData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
          rating
        }
        customerData {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          PaypalID
          OnesignalID
        }
        customer
        store
        storeData
        deliveryAddressData {
          _id
          formatted_address
          puertaPiso
          city
          postalcode
          type
          usuario
          lat
          lgn
        }
        deliveryAddress
        orderIsAlreadyPaid
        payment
        note
        items
        numberOfCustomers
        deliveryCost
        serviceCharge
        discountTotal
        IntegerValue
        paymentMethod
        statusProcess
        Needcutlery
        status
        invoiceUrl
      }
    }
  }
`;

const NEW_ORDER_PROCEED = gql`
  mutation NewOrdenProceed(
    $ordenId: String
    $status: String
    $IntegerValue: Int
    $statusProcess: OrderProccessInput
  ) {
    NewOrdenProceed(
      ordenId: $ordenId
      status: $status
      IntegerValue: $IntegerValue
      statusProcess: $statusProcess
    ) {
      messages
      success
    }
  }
`;

const ACTUALIZAR_CUSTON_ORDER = gql`
  mutation actualizarOrderProcess($input: CustonOrderInput) {
    actualizarOrderProcess(input: $input) {
      success
      messages
      data {
        id
      }
    }
  }
`;

const ACTUALIZAR_STORE = gql`
  mutation actualizarRestaurant($input: RestaurantInput) {
    actualizarRestaurant(input: $input) {
      messages
      success
    }
  }
`;

const CREAR_COMMENT = gql`
  mutation crearValoracion($input: ValoracionCreationInput) {
    crearValoracion(input: $input) {
      id
      user
      comment
      value
      restaurant
    }
  }
`;

const UPLOAD_FILE_STORE = gql`
  mutation singleUploadToStoreImagenAws($file: String) {
    singleUploadToStoreImagenAws(file: $file) {
      data
    }
  }
`;

const CREATED_STORE = gql`
  mutation createRestaurant($input: DataStore) {
    createRestaurant(input: $input) {
      messages
      success
      data {
        _id
      }
    }
  }
`;

const UPDATE_STORE = gql`
  mutation actualizarRestaurantAdmin($input: DataStore) {
    actualizarRestaurantAdmin(input: $input) {
      messages
      success
    }
  }
`;

const ACTUALIZAR_RIDER = gql`
  mutation actualizarRiders($input: ActualizarUsuarioInput) {
    actualizarRiders(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
    }
  }
`;

const CREATE_RIDER = gql`
  mutation crearRiders($input: UsuarioInput) {
    crearRiders(input: $input) {
      message
      success
    }
  }
`;

const DELETED_RIDER = gql`
  mutation eliminarRiders($id: ID!) {
    eliminarRiders(id: $id) {
      messages
      success
    }
  }
`;

const ASING_RIDERS = gql`
  mutation NewOrdenAsigRider($ordenId: String, $riderID: String) {
    NewOrdenAsigRider(ordenId: $ordenId, riderID: $riderID) {
      messages
      success
    }
  }
`;

const UPDATE_MESSAGE = gql`
  mutation updateMessageHome($input: JSON) {
    updateMessageHome(input: $input) {
      messages
      success
    }
  }
`;

const CREATED_MESSAGE = gql`
  mutation createMessageHome($input: JSON) {
    createMessageHome(input: $input) {
      messages
      success
    }
  }
`;

const CREATED_ADS = gql`
  mutation createAds($data: AdsData) {
    createAds(data: $data) {
      messages
      success
    }
  }
`;

const UPDATE_ADS = gql`
  mutation updateAds($data: JSON) {
    updateAds(data: $data) {
      messages
      success
    }
  }
`;

const DELETED_ADS = gql`
  mutation eliminarAds($id: String) {
    eliminarAds(id: $id) {
      messages
      success
    }
  }
`;

const UPDATE_RESERVA = gql`
  mutation NewReservaProceed($id: ID, $status: String) {
    NewReservaProceed(id: $id, status: $status) {
      messages
      success
    }
  }
`;
export const mutation = {
  UPDATE_RESERVA,
  AUTENTICAR_ADMIN,
  NUEVO_ADMIN,
  ACTUALIZAR_ADMIN,
  UPLOAD_FILE,
  ELIMINAR_ADMIN,
  CREATE_NEW_ORDER,
  NEW_ORDER_PROCEED,
  ACTUALIZAR_CUSTON_ORDER,
  ACTUALIZAR_STORE,
  CREAR_COMMENT,
  UPLOAD_FILE_STORE,
  CREATED_STORE,
  UPDATE_STORE,
  ACTUALIZAR_RIDER,
  CREATE_RIDER,
  DELETED_RIDER,
  ASING_RIDERS,
  UPDATE_MESSAGE,
  CREATED_MESSAGE,
  CREATED_ADS,
  UPDATE_ADS,
  DELETED_ADS,
};
