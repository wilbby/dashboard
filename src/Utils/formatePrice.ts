export const formaterPrice = (
  price: any,
  language: string,
  currency: string
) => {
  const prices = new Intl.NumberFormat(language ? language : "es-ES", {
    style: "currency",
    currency: currency ? currency : "EUR",
  }).format(price);
  return prices;
};
