import React, { useState } from "react";
import "./index.css";
import { Layout, Menu, Breadcrumb, Modal, Button } from "antd";
import {
  UserOutlined,
  CalendarOutlined,
  NotificationOutlined,
  ExclamationCircleOutlined,
  ShoppingOutlined,
  ShopOutlined,
} from "@ant-design/icons";
import Logo from "../../Assets/image/logowhite.png";
import { withRouter } from "react-router-dom";
import MyData from "../MyData";
import { useQuery } from "react-apollo";
import { query } from "../../GraphQL";

/// Screnn ///
import Order from "../Orders";
import OrderExpress from "../OrdersExpress";
import Customer from "../Customers";
import Mensage from "../messages";
import Riders from "../Riders";
import Establecimientos from "../Establecimientos";
import AddRating from "../Establecimientos/addRating/addRating";
import Avisos from "../Avisos";
import Publicidad from "../Publicidad";
import Report from "../Report";
import Reservas from "../Reservas";
import Mercadona from "../Establecimientos/Mercadona";
import StripeConnetc from "../StripeConnect";
// End ///

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

function HomeComponent(props: any) {
  const [index, setIndex] = useState(1);
  const [activeIndex, setActiveIndex] = useState(3);
  const [visible, setvisible] = useState(false);
  const { history } = props;

  const { data, refetch } = useQuery(query.USER_DETAIL);

  const user = data && data.getUsuarioAdmin ? data.getUsuarioAdmin.data : {};

  const CerrarSesion = () => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que deseas cerrar sesión?",
      icon: <ExclamationCircleOutlined />,
      content: "Esperamos verte pronto por el equipo de Wilbby",
      cancelText: "Cancelar",
      okText: "Cerrar sesión",
      onOk() {
        localStorage.removeItem("id");
        localStorage.removeItem("token");
        localStorage.removeItem("city");
        history.push(`/`);
      },
      onCancel() {},
    });
  };

  let tipe: string;

  switch (index) {
    case 1:
      tipe = "Clientes";
      break;
    case 2:
      tipe = "Riders";
      break;
    case 3:
      tipe = "Parners";
      break;

    default:
      tipe = "Clientes";
      break;
  }

  return (
    <Layout>
      <Header className="header">
        <div className="logo_main">
          <img src={Logo} alt="Wilbby" />
        </div>

        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item
            key="1"
            onClick={(value) => {
              setIndex(Number(value.key));
              setActiveIndex(3);
            }}
          >
            Clientes
          </Menu.Item>
          <Menu.Item key="2" onClick={(value) => setIndex(Number(value.key))}>
            Riders
          </Menu.Item>
          <Menu.Item
            key="3"
            onClick={(value) => {
              setIndex(Number(value.key));
              setActiveIndex(11);
            }}
          >
            Partners
          </Menu.Item>
        </Menu>

        <div className="logout">
          <div className="my-user" onClick={() => setvisible(true)}>
            <UserOutlined style={{ fontSize: 20, color: "white" }} />
          </div>
          <Modal
            title={null}
            visible={visible}
            footer={false}
            onCancel={() => setvisible(false)}
          >
            <MyData user={user} refetch={refetch} setvisible={setvisible} />
          </Modal>
          <button onClick={() => CerrarSesion()}>Cerrar sesión</button>
        </div>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>{localStorage.getItem("city")}</Breadcrumb.Item>
          <Breadcrumb.Item>{tipe}</Breadcrumb.Item>
        </Breadcrumb>

        {index === 1 ? (
          <Layout
            className="site-layout-background"
            style={{ padding: "24px 0" }}
          >
            <Sider className="site-layout-background" width={200}>
              <Menu
                mode="inline"
                defaultSelectedKeys={["3"]}
                defaultOpenKeys={["sub2"]}
                style={{ height: "100%" }}
              >
                <SubMenu key="sub1" icon={<UserOutlined />} title="Clientes">
                  <Menu.Item
                    key="2"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Clientes
                  </Menu.Item>
                  <Menu.Item
                    key="1"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Enviar Mensages
                  </Menu.Item>
                  <Menu.Item
                    key="100"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Generar reporte
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub3"
                  icon={<CalendarOutlined />}
                  title="Reservas"
                >
                  <Menu.Item
                    key="90"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Reservas
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<ShoppingOutlined />} title="Pedidos">
                  <Menu.Item
                    key="3"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Todos los pedidos
                  </Menu.Item>
                  <Menu.Item
                    key="10"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Tomelloso
                  </Menu.Item>
                  <Menu.Item
                    key="4"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Alcázar de San Juan
                  </Menu.Item>
                  <Menu.Item
                    key="5"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Valdepeñas
                  </Menu.Item>

                  <Menu.Item
                    key="7"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Quintanar de la Orden
                  </Menu.Item>

                  <Menu.Item
                    key="57"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Campo de Criptana
                  </Menu.Item>

                  <Menu.Item
                    key="70"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Almansa
                  </Menu.Item>

                  <Menu.Item
                    key="9"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Pedido Express
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub3"
                  icon={<NotificationOutlined />}
                  title="Notificaciones"
                >
                  <Menu.Item
                    key="20"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Avisos
                  </Menu.Item>
                  <Menu.Item
                    key="21"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Publicidad
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
              {activeIndex === 1 ? <Mensage /> : null}
              {activeIndex === 2 ? <Customer /> : null}
              {activeIndex === 9 ? <OrderExpress /> : null}
              {activeIndex === 3 ? <Order citys={null} /> : null}
              {activeIndex === 4 ? <Order citys="Alcázar de San Juan" /> : null}
              {activeIndex === 5 ? <Order citys="Valdepeñas" /> : null}
              {activeIndex === 70 ? <Order citys="Almansa" /> : null}
              {activeIndex === 7 ? (
                <Order citys="Quintanar de la Orden" />
              ) : null}
              {activeIndex === 57 ? <Order citys="Campo de Criptana" /> : null}

              {activeIndex === 10 ? <Order citys="Tomelloso" /> : null}
              {activeIndex === 20 ? <Avisos /> : null}
              {activeIndex === 21 ? <Publicidad /> : null}
              {activeIndex === 100 ? <Report /> : null}
              {activeIndex === 90 ? <Reservas /> : null}
            </Content>
          </Layout>
        ) : null}

        {index === 2 ? (
          <Layout
            className="site-layout-background"
            style={{ padding: "24px 0" }}
          >
            <Sider className="site-layout-background" width={200}>
              <Menu
                mode="inline"
                defaultSelectedKeys={["1"]}
                defaultOpenKeys={["sub1"]}
                style={{ height: "100%" }}
              >
                <SubMenu
                  key="sub1"
                  icon={<UserOutlined />}
                  title="Repartidores"
                >
                  <Menu.Item key="8">Repartidores</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
              <Riders />
            </Content>
          </Layout>
        ) : null}

        {index === 3 ? (
          <Layout
            className="site-layout-background"
            style={{ padding: "24px 0" }}
          >
            <Sider className="site-layout-background" width={200}>
              <Menu
                mode="inline"
                defaultSelectedKeys={["1"]}
                defaultOpenKeys={["sub1"]}
                style={{ height: "100%" }}
              >
                <SubMenu key="sub1" icon={<ShopOutlined />} title="Partners">
                  <Menu.Item
                    key="11"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Establecimientos
                  </Menu.Item>

                  <Menu.Item
                    key="111"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Stripe Connect
                  </Menu.Item>
                  <Menu.Item
                    key="12"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Añadir valoración
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub2"
                  icon={<ShoppingOutlined />}
                  title="Productos"
                >
                  <Menu.Item
                    key="13"
                    onClick={(value) => setActiveIndex(Number(value.key))}
                  >
                    Añadir productos
                  </Menu.Item>
                  <Menu.Item key="14">Menu</Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub3"
                  icon={<NotificationOutlined />}
                  title="Incidecias"
                >
                  <Menu.Item key="14">Modificaciones</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
              {activeIndex === 11 ? <Establecimientos /> : null}
              {activeIndex === 12 ? <AddRating /> : null}
              {activeIndex === 13 ? <Mercadona /> : null}
              {activeIndex === 111 ? <StripeConnetc /> : null}
            </Content>
          </Layout>
        ) : null}
      </Content>
      <Footer style={{ textAlign: "center" }}>
        Wilbbby App ©2021 Created by Technology Team Wilbby
      </Footer>
    </Layout>
  );
}

export default withRouter(HomeComponent);
