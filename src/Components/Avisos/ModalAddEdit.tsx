import { useState } from "react";
import { Modal, Input, Switch, Select, message } from "antd";
import dataMunicipio from "../Orders/municipio.json";
import { mutation } from "../../GraphQL";
import { useMutation } from "react-apollo";

const { TextArea } = Input;
const { Option } = Select;

export default function ModalAddEdit({
  data,
  visible,
  setVisible,
  edit,
  getdata,
  setDataModal,
}: any) {
  const [updateMessageHome] = useMutation(mutation.UPDATE_MESSAGE);
  const [createMessageHome] = useMutation(mutation.CREATED_MESSAGE);

  const act = edit ? data.active : false;

  const [active, setActive] = useState(act);
  const [title, setTitle] = useState(edit ? data.title : "");
  const [messages, setMessage] = useState(edit ? data.message : "");
  const [city, setcity] = useState(edit ? data.city : null);
  const [loading, setLoading] = useState(false);

  const isOk = () => {
    if (title && message && city) {
      return false;
    } else {
      return true;
    }
  };

  const createdMessage = () => {
    setLoading(true);
    const input = {
      title: title,
      message: messages,
      city: city,
      active: active,
    };

    createMessageHome({ variables: { input } })
      .then((res) => {
        const done = res.data.createMessageHome;
        if (done.success) {
          getdata(city);
          message.success("Mensaje creado con exito");
          setLoading(false);
          setVisible(false);
          setDataModal(null);
        } else {
          message.warning("Algo salio mal");
          setLoading(false);
        }
      })
      .catch(() => {
        message.error("Algo salio mal");
        setLoading(false);
      });
  };

  const updateMessage = () => {
    setLoading(true);
    const input = {
      _id: data._id,
      title: title,
      message: messages,
      city: city,
      active: active,
    };

    updateMessageHome({ variables: { input } })
      .then((res) => {
        const done = res.data.updateMessageHome;

        if (done.success) {
          getdata(city);
          message.success("Mensaje actualizado con exito");
          setLoading(false);
          setVisible(false);
          setDataModal(null);
        } else {
          message.warning("Algo salio mal");
          setLoading(false);
        }
      })
      .catch(() => {
        message.error("Algo salio mal");
        setLoading(false);
      });
  };

  return (
    <Modal
      visible={visible}
      onCancel={() => {
        setVisible(false);
        setDataModal(null);
      }}
      width={400}
      onOk={() => {
        if (edit) {
          updateMessage();
        } else {
          createdMessage();
        }
      }}
      okButtonProps={{ disabled: isOk(), loading: loading }}
      okText="Guardar cambios"
      cancelText="Cancelar"
    >
      <div style={{ margin: 15 }}>
        <Input
          placeholder="Título"
          defaultValue={title}
          onChange={(value: any) => setTitle(value.target.value)}
        />
        <TextArea
          rows={4}
          defaultValue={messages}
          placeholder="Mensaje"
          maxLength={1500}
          style={{ marginTop: 10 }}
          onChange={(value: any) => setMessage(value.target.value)}
        />

        <Select
          defaultValue={city}
          showSearch
          style={{ width: "100%", marginTop: 10 }}
          placeholder="Selectcciona una ciudad"
          optionFilterProp="children"
          allowClear
          onChange={(value: any) => setcity(value)}
          filterOption={(input: any, option: any) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {dataMunicipio.map((data, i) => {
            return (
              <Option value={data.nombre} key={i}>
                {data.nombre}
              </Option>
            );
          })}
        </Select>

        <Switch
          checkedChildren="Activado"
          unCheckedChildren="Desactivado"
          defaultChecked={active}
          onChange={(checked) => setActive(checked)}
          style={{ marginTop: 10 }}
        />
      </div>
    </Modal>
  );
}
