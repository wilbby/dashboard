import { useState, useEffect } from "react";
import "./index.css";
import {  Select, Button, Empty } from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";
import { LOCAL_API_URL } from "../../Config";
import Items from "./item";
import ModalAddEdit from "./ModalAddEdit";
import dataMunicipio from "../Orders/municipio.json";

const initialState = {
  _id: null,
  city: null,
  title: null,
  message: null,
  active: false,
};

export default function Avisos() {
  const [city, setCity] = useState("Tomelloso");
  const [messageHome, setmessageHome] = useState(initialState);
  const [visible, setVisible] = useState(false);
  const [dataModal, setDataModal] = useState(null);

  const { Option } = Select;

  const getData = (value: string) => {
    fetch(`${LOCAL_API_URL}/get-notification-home?city=${value}`).then(
      async (respuesta) => {
        const homeData = await respuesta.json();
        if (homeData.success) {
          if (homeData.data) {
            setmessageHome(homeData.data);
          } else {
            setmessageHome(initialState);
          }
        }
      }
    );
  };

  useEffect(() => {
    getData(city);
  }, [city]);

  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
      <Select
          defaultValue={city}
          showSearch
          style={{ width: 300 }}
          placeholder="Selectcciona una ciudad"
          optionFilterProp="children"
          allowClear
          onChange={(value: any) =>{ setCity(value)
            getData(value)}}
          filterOption={(input: any, option: any) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {dataMunicipio.map((data, i) => {
            return (
              <Option value={data.nombre} key={i}>
                {data.nombre}
              </Option>
            );
          })}
        </Select>
        <Button
          icon={<PlusCircleOutlined />}
          type="primary"
          onClick={() => setVisible(true)}
        >
          Añadir mensage
        </Button>
      </div>
      <div style={{ marginTop: 80 }}>
        {messageHome.title ? (
          <Items data={messageHome} getdata={getData} />
        ) : (
          <Empty description="No hay mensaje en esta ciudad" />
        )}
      </div>
      <ModalAddEdit
        data={initialState}
        visible={visible}
        setVisible={setVisible}
        edit={false}
        getdata={getData}
        setDataModal={setDataModal}
      />
    </>
  );
}
