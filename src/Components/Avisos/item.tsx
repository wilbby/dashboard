import { useState } from "react";
import "./index.css";
import Lottie from "react-lottie";
import source from "../../Assets/animate/chica.json";
import { Button, Tag } from "antd";
import {
  EnvironmentOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
} from "@ant-design/icons";
import ModalAddEdit from "./ModalAddEdit";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: source,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Item({ data, getdata }: any) {
  const [visible, setVisible] = useState(false);
  const [dataModal, setDataModal] = useState(null);

  const openData = (datas: any) => {
    setVisible(true);
    setDataModal(datas);
  };

  return (
    <div className="items_mensage">
      <div className="items_mensage_cont">
        <div>
          <Lottie options={defaultOptions} width={60} height={55} />
        </div>
        <div style={{ marginLeft: 10 }}>
          <h1>{data.title}</h1>
          <p>{data.message}</p>
          <p>
            <EnvironmentOutlined /> {data.city}
          </p>
        </div>
      </div>
      <div
        style={{
          marginLeft: 70,
          marginTop: 10,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Button type="primary" onClick={() => openData(data)}>
          Editar mensaje
        </Button>
        {data.active ? (
          <Tag icon={<CheckCircleOutlined />} color="success">
            Activo
          </Tag>
        ) : (
          <Tag icon={<CloseCircleOutlined />} color="error">
            Desactivado
          </Tag>
        )}
      </div>
      {dataModal ? (
        <ModalAddEdit
          data={dataModal}
          visible={visible}
          setVisible={setVisible}
          setDataModal={setDataModal}
          edit={true}
          getdata={getdata}
        />
      ) : null}
    </div>
  );
}
