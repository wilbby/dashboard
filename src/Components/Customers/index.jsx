import React, { useState } from "react";
import { List, Avatar, Button, Skeleton, Input, Statistic } from "antd";
import "./index.css";
import { Query } from "react-apollo";
import { query } from "../../GraphQL";
import { IMAGES_PATH } from "../../Config";

const { Search } = Input;

export default function Customer() {
  const [page, setpage] = useState(1);
  const [limit] = useState(20);
  const [search, setsearch] = useState("");

  const loadMore = () => {
    return (
      <div
        style={{
          textAlign: "center",
          marginTop: 12,
          height: 32,
          lineHeight: "32px",
        }}
      >
        {!search ? (
          <Button onClick={() => setpage(page + 1)}>Ver más clientes</Button>
        ) : null}
        {search ? (
          <Button onClick={() => setsearch("")} style={{ marginLeft: 15 }}>
            Ver todos
          </Button>
        ) : null}
        {page > 1 ? (
          <Button onClick={() => setpage(1)} style={{ marginLeft: 15 }}>
            Volver al inicio
          </Button>
        ) : null}
      </div>
    );
  };

  const searchCustomer = (email) => {
    setsearch(email);
  };

  return (
    <>
      <Query
        query={query.CUSTOMERS}
        variables={{ page: page, limit: limit, email: search }}
      >
        {(response) => {
          if (response) {
            const customers =
              response && response.data && response.data.getCustomers
                ? response.data.getCustomers.data
                : [];

            const count =
              response && response.data && response.data.getCustomers
                ? response.data.getCustomers.count
                : 0;
            response.refetch();
            return (
              <div>
                <Statistic
                  title="Total usuarios"
                  value={count}
                  style={{ marginBottom: 20 }}
                />
                <div style={{ marginBottom: 30 }}>
                  <Search
                    placeholder="Buscar usuario por email"
                    enterButton="Buscar"
                    size="large"
                    loading={response.loading}
                    onSearch={(value) => searchCustomer(value)}
                    allowClear
                  />
                </div>
                <List
                  className="demo-loadmore-list"
                  loading={response.loading}
                  itemLayout="horizontal"
                  loadMore={loadMore}
                  dataSource={customers}
                  renderItem={(item) => {
                    return (
                      <List.Item
                        actions={[
                          <a key="list-loadmore-edit">Edita</a>,
                          <a key="list-loadmore-more">Ver detalles</a>,
                        ]}
                      >
                        <Skeleton
                          avatar
                          title={false}
                          loading={response.loading}
                          active
                        >
                          <List.Item.Meta
                            avatar={<Avatar src={IMAGES_PATH + item.avatar} />}
                            title={
                              <p>
                                {item.name} {item.lastName}
                              </p>
                            }
                            description={item.email}
                          />
                          <div>
                            <p>Teléfono: +{item.telefono}</p>
                            <p>Notificación ID: {item.OnesignalID}</p>
                          </div>
                        </Skeleton>
                      </List.Item>
                    );
                  }}
                />
              </div>
            );
          }
        }}
      </Query>
    </>
  );
}
