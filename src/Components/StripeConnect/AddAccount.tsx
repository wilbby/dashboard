import React, { useState } from "react";
import "./index.css";
import { Button, Input, message, Select, Modal } from "antd";
import { isValidIBANNumber } from "./alidateIBAN";
import { LOCAL_API_URL } from "../../Config";

const { Option } = Select;
const { TextArea } = Input;

const business_type_array = [
  {
    id: "company",
    name: "Empresa",
  }
];

export default function AddStripeConnetc({ visible, handleCancel, getDatos }: any) {
  const [email, setemail] = useState("");
  const [business_type, setbusiness_type] = useState("company");
  const [city, setcity] = useState("");
  const [line1, setline1] = useState("");
  const [postal_code, setpostal_code] = useState("");
  const [state, setstate] = useState("");
  const [name, setname] = useState("");
  const [phone, setphone] = useState("");
  const [cif, setcif] = useState("");
  const [description, setdescription] = useState("");
  const [url, seturl] = useState("");
  const [iban, setiban] = useState("");
  const [loading, setLoading] = useState(false);

  const input = {
    email: email,
    business_type: business_type,
    city: city,
    line1: line1,
    postal_code: postal_code,
    state: state,
    name: name,
    phone: phone,
    cif: cif,
    description: description,
    url: url,
    iban: iban,
  };

  const onChange = (value: any) => {
    setbusiness_type(value);
  };

  const isOK = () => {
    if (
      email &&
      business_type &&
      city &&
      line1 &&
      postal_code &&
      state &&
      name &&
      phone &&
      cif &&
      description &&
      url &&
      isValidIBANNumber(iban)
    ) {
      if (isValidIBANNumber(iban)) {
        return false;
      } else {
        message.error("Por favor introduce un IBAN valido");
      }
    } else {
      return true;
    }
  };

  const createAccount = async () => {
    setLoading(true);
    const resp = await fetch(`${LOCAL_API_URL}/create-stripe-connect`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ data: input }),
    });

    const response = await resp.json();

    if(resp.ok) {
    if (response.success) {
      message.success("La cuenta fue creada con éxito");
      getDatos()
      setTimeout(() => {
        setLoading(false);
        handleCancel();
      }, 1000);
    } else {
      setLoading(false);
      message.error("Algo salio mal intentalo de nuevo");
    }

  } else{
    setLoading(false);
    message.error("Algo salio mal intentalo de nuevo");
  }
  };

  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      onOk={createAccount}
      title="Crear empresa"
      okButtonProps={{ disabled: isOK(), loading: loading }}
      okText="Crear empres"
      cancelText="Cancelar"
      width={500}
    >
      <div className="rating_content" style={{ marginTop: 20 }}>
        <div className="rating_content_form">
          <div>
            <p>Tipo de negocio</p>
            <Select
              showSearch
              style={{ width: 360 }}
              placeholder="Selecciona el tipo de negocio"
              optionFilterProp="children"
              defaultValue={business_type}
              allowClear
              onChange={onChange}
              filterOption={(input: any, option: any) =>
                //@ts-ignore
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {business_type_array.map((data: any, i: any) => {
                return (
                  <Option value={data.id} key={i}>
                    {data.name}
                  </Option>
                );
              })}
            </Select>
          </div>

          <div style={{ marginTop: 20 }}>
            <p>Datos de la empresa</p>
            <Input
              onChange={(e) => setname(e.target.value)}
              placeholder="Nombre de la empresa"
            />
            <Input
              onChange={(e) => setcif(e.target.value)}
              placeholder="CIF"
              style={{ marginTop: 10 }}
            />
            <Input
              onChange={(e) => setemail(e.target.value)}
              placeholder="E-mail"
              style={{ marginTop: 10 }}
            />
            <Input
              onChange={(e) => setphone(e.target.value)}
              placeholder="Teléfono"
              style={{ marginTop: 10 }}
            />
            <Input
              onChange={(e) => seturl(e.target.value)}
              placeholder="Web"
              style={{ marginTop: 10 }}
            />

            <TextArea
              rows={4}
              allowClear={true}
              placeholder="Descripción"
              onChange={(e) => setdescription(e.target.value)}
              style={{ marginTop: 10 }}
            />
          </div>

          <div style={{ marginTop: 20 }}>
            <p>Dirección de la empresa</p>
            <Input
              onChange={(e) => setline1(e.target.value)}
              placeholder="Nombre de la calle"
            />
            <Input
              onChange={(e) => setcity(e.target.value)}
              placeholder="Ciudad"
              style={{ marginTop: 10 }}
            />
            <Input
              onChange={(e) => setpostal_code(e.target.value)}
              placeholder="Código postal"
              style={{ marginTop: 10 }}
            />
            <Input
              onChange={(e) => setstate(e.target.value)}
              placeholder="Comunidad autónoma"
              style={{ marginTop: 10 }}
            />
          </div>

          <div style={{ marginTop: 20 }}>
            <p>Datos de pago</p>
            <Input
              onChange={(e) => setiban(e.target.value)}
              placeholder="IBAN"
            />
          </div>
        </div>
      </div>
    </Modal>
  );
}
