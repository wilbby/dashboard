import React, { useState, useEffect } from "react";
import { Space, Table, Tag, Button, Popconfirm, message } from "antd";
import type { ColumnsType } from "antd/es/table";
import { PlusCircleOutlined } from "@ant-design/icons";
import AddStripeConnetc from "./AddAccount";
import { LOCAL_API_URL } from "../../Config";

export default function StripeConnetc() {
  const [visible, setvisible] = useState(false);
  const [datos, setDatos] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleCancel = () => {
    setvisible(false);
  };

  const getDatos = async () => {
    setLoading(true);
    const resp = await fetch(`${LOCAL_API_URL}/stripe-connect`, {
      method: "GET",
    });
    const response = await resp.json();
    if (response.success) {
      setLoading(false);
      setDatos(response.data);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    getDatos();
  }, []);

  const renderStatus = (status: string) => {
    switch (status) {
      case "company":
        return <Tag color="purple">{status}</Tag>;
      case "individual":
        return <Tag color="orange">{status}</Tag>;
      case "non_profit":
        return <Tag color="lime">{status}</Tag>;
      case "government_entity":
        return <Tag color="red">{status}</Tag>;
      default:
        return <Tag color="purple">{status}</Tag>;
    }
  };

  const status = (status: string) => {
    if (status === "inactive") {
      return <Tag color="red">{status}</Tag>;
    } else {
      return <Tag color="lime">{status}</Tag>;
    }
  };

  const deletes = async (id: string) => {
    setLoading(true);
    const resp = await fetch(`${LOCAL_API_URL}/stripe-connect-delete`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id }),
    });

    const response = await resp.json();

    if (response.success) {
      getDatos();
      message.success("La cuenta fue eliminada con éxito");
      setTimeout(() => {
        getDatos();
        setLoading(false);
      }, 1000);
    } else {
      setLoading(false);
      message.error("Algo salio mal intentalo de nuevo");
    }
  };

  const columns: ColumnsType<any> = [
    {
      title: "Nombre",
      key: "name",
      render: (data) => (
        <div>
          <p style={{ margin: 0 }}>{data.company.name}</p>
          <p style={{ margin: 0 }}>{data.email}</p>
          <p style={{ margin: 0 }}>{data.company.phone}</p>
        </div>
      ),
    },
    {
      title: "CIF",
      key: "age",
      render: (data) => {
        return <p>{data.company.registration_number}</p>
      },
    },

    {
      title: "IBAN",
      key: "age",
      render: (data) => (
        <div>
          <p style={{ margin: 0 }}>{data.external_accounts.data[0].last4}</p>
          <p style={{ margin: 0 }}>
            {data.external_accounts.data[0].bank_name}
          </p>
        </div>
      ),
    },
    {
      title: "Dirección",
      key: "address",
      render: (data) => (
        <div>
          <p style={{ margin: 0 }}>{data.company.address.line1}</p>
          <p style={{ margin: 0 }}>{data.company.address.postal_code}</p>
          <p style={{ margin: 0 }}>{data.company.address.city}</p>
        </div>
      ),
    },
    {
      title: "Tipo",
      key: "tags",
      render: (data) => {
        return renderStatus(data.business_type);
      },
    },

    {
      title: "Estado",
      key: "status",
      render: (data) => {
        return status(data.capabilities.transfers);
      },
    },
    {
      title: "Acción",
      key: "action",
      render: (data) => (
        <Space size="middle">
          <Popconfirm
            title="¿Estás seguro que deseas eliminar esta cuenta?"
            onConfirm={()=> deletes(data.id)}
            onCancel={() => {}}
            okText="Eliminar"
            cancelText="Cancelar"
          >
            <Button type="link" danger>
              Eliminar
            </Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <div style={{ margin: "30px 0px" }}>
        <Button
          type="primary"
          icon={<PlusCircleOutlined />}
          style={{ borderRadius: 10 }}
          onClick={() => setvisible(true)}
        >
          Crear cuenta
        </Button>
      </div>
      <Table columns={columns} dataSource={datos} loading={loading} />
      <AddStripeConnetc visible={visible} handleCancel={handleCancel} getDatos={getDatos} />
    </div>
  );
}
