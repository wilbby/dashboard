import React, { useState } from "react";
import { Input, Button, message } from "antd";
import "./index.css";
import { LOCAL_API_URL } from "../../Config/index";

const { TextArea } = Input;

export default function Mensage() {
  const [onesignalID, setonesignalID] = useState("");
  const [title, settitle] = useState("");
  const [messages, setmessage] = useState("");
  const [mensageMovil, setmensageMovil] = useState("");
  const [telefono, settelefono] = useState("");

  const sendSMS = async () => {
    const input = {
      mensageMovil: mensageMovil,
      telefono: telefono,
    };
    if (mensageMovil && telefono) {
      let res = await fetch(`${LOCAL_API_URL}/send-sms-to-client`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(input),
      });
      if (res) {
        const user = await res.json();
        if (user.success) {
          message.success("Notificación enviada con éxito");
        } else {
          message.error("Algo salio mal intentalo de nuevo");
        }
      }
    } else {
      message.warning("Debes completar todos los campos para continuar");
    }
  };

  const sendNotification = async () => {
    const input = {
      onesignalID: onesignalID,
      title: title,
      messages: messages,
    };
    if (onesignalID && title && messages) {
      let res = await fetch(`${LOCAL_API_URL}/send-notification`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(input),
      });
      if (res) {
        const user = await res.json();
        if (user.success) {
          message.success("Notificación enviada con éxito");
        } else {
          message.error("Algo salio mal intentalo de nuevo");
        }
      }
    } else {
      message.warning("Debes completar todos los campos para continuar");
    }
  };

  return (
    <div className="cont_mensages">
      <div className="Notificaio_Push">
        <h1>Notificación Push</h1>
        <Input
          placeholder="Id de Notificación"
          onChange={(e) => setonesignalID(e.target.value)}
        />

        <Input
          placeholder="Título"
          onChange={(e) => settitle(e.target.value)}
          style={{ marginTop: 20 }}
        />

        <TextArea
          showCount
          maxLength={150}
          placeholder="Mensage"
          style={{ marginTop: 20 }}
          onChange={(e) => setmessage(e.target.value)}
        />

        <Button
          type="primary"
          style={{ marginTop: 20 }}
          onClick={() => sendNotification()}
        >
          Enviar Mensaje
        </Button>
      </div>

      <div className="Notificaio_Push">
        <h1>Mensaje de texto</h1>
        <Input
          defaultValue="34"
          style={{ marginTop: 20 }}
          placeholder="Número teléfono"
          onChange={(e) => settelefono(e.target.value)}
        />

        <TextArea
          showCount
          maxLength={150}
          placeholder="Mensage"
          style={{ marginTop: 20 }}
          onChange={(e) => setmensageMovil(e.target.value)}
        />

        <Button
          type="primary"
          style={{ marginTop: 20 }}
          onClick={() => sendSMS()}
        >
          Enviar Mensaje
        </Button>
      </div>
    </div>
  );
}
