import React from "react";
import animationData from "../../Assets/animate/chica.json";
import Lottie from "react-lottie";
import "./index.css";

export default function nodata(props: any) {
  const { title, imgen } = props;

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: imgen ? imgen : animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div className="noData">
      <Lottie options={defaultOptions} height={130} width={150} />
      <p>{title}</p>
    </div>
  );
}
