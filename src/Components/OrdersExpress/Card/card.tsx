import React from "react";
import {
  ExclamationCircleOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
  RollbackOutlined,
  CheckOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import "./card.css";
import { Modal, Table, Tag, Button, message } from "antd";
import { mutation } from "../../../GraphQL";
import { useMutation } from "react-apollo";
import Details from "../DetailsCard";

const { confirm } = Modal;

export default function Card(props: any) {
  const {
    loading,
    datos,
    openRider,
    setpage,
    setlimit,
    onChangePage,
    refetch,
    SendPushNotificationNewordenUSer,
  } = props;

  const [actualizarOrderProcess] = useMutation(
    mutation.ACTUALIZAR_CUSTON_ORDER
  );

  const returOrder = (orders: any) => {
    const input = {
      id: orders.id,
      riders: orders.riders,
      estado: "Devuelto",
      status: "exception",
      progreso: "0",
    };
    actualizarOrderProcess({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarOrderProcess.success) {
        refetch();
        message.success("El pedido ha sido marcado como devuelto");
      }
    });
  };

  function showDeleteConfirm(data: any) {
    confirm({
      title: "¿Estás seguro que deseas hacer la devolución?",
      icon: <ExclamationCircleOutlined />,
      content: "Lamentamos no poder terminar este pedido",
      okText: "Devolver",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        returOrder(data);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const columns = [
    {
      title: "Orden ID",
      dataIndex: "",
      key: "",
      render: (d: any) => {
        return <Tag color="#f50">#{d.display_id}</Tag>;
      },
    },
    {
      title: "Cliente",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        return (
          <div>
            <p style={{ margin: 0, padding: 0 }}>
              {d.fronStore
                ? `${d.Store.title}`
                : `${d.usuario.name ? d.usuario.name : "Usuario"} ${
                    d.usuario.lastName ? d.usuario.lastName : "Invitado"
                  }`}
            </p>
            <span style={{ color: "gray" }}>{d.city}</span>
            <br />
            {d.fronStore ? (
              <Tag color="#007e06">Pedido desde un partner</Tag>
            ) : null}
          </div>
        );
      },
    },
    {
      title: "Rider",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        return (
          <div>
            {d.riders ? (
              <Button
                type="primary"
                onClick={() => openRider(d)}
                disabled={d.estado === "Devuelto" ? true : false}
              >
                {d.Riders.name} {d.Riders.lastName}
              </Button>
            ) : (
              <Button
                type="primary"
                onClick={() => openRider(d)}
                disabled={d.estado === "Devuelto" ? true : false}
              >
                Asignar
              </Button>
            )}
          </div>
        );
      },
    },
    {
      title: "Estado",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        let color = "";
        switch (d.estado) {
          case "Pendiente de pago":
            color = "#FFA500";
            break;
          case "Rechazada por el rider":
            color = "#F5365C";
            break;
          case "Rechazada por la tienda":
            color = "#F5365C";
            break;
          case "Resolución":
            color = "#FFA500";
            break;
          case "Devuelto":
            color = "#F5365C";
            break;
          case "Nueva":
            color = "#90c33c";
            break;
          default:
            color = "#90c33c";
            break;
        }

        const renderStatus = () => {
          switch (d.estado) {
            case "Rechazada":
              return (
                <Tag icon={<CloseCircleOutlined />} color="error">
                  {d.estado}
                </Tag>
              );
            case "Devuelto":
              return (
                <Tag icon={<RollbackOutlined />} color="error">
                  {d.estado}
                </Tag>
              );
            case "Pagado":
              return (
                <Tag icon={<CheckOutlined />} color="warning">
                  {d.estado}
                </Tag>
              );

            case "Entregado":
              return (
                <Tag icon={<CheckCircleOutlined />} color="success">
                  {d.estado}
                </Tag>
              );

            case "Valorada":
              return (
                <Tag icon={<CheckCircleOutlined />} color="success">
                  {d.estado}
                </Tag>
              );
            default:
              return (
                <Tag icon={<SyncOutlined spin />} color="processing">
                  {d.estado}
                </Tag>
              );
          }
        };
        return <div>{renderStatus()}</div>;
      },
    },
    {
      title: "Devolver",
      dataIndex: "",
      key: "x",
      render: (data: any) => {
        return (
          <Button
            danger
            type="primary"
            onClick={() => showDeleteConfirm(data)}
            disabled={data.estado === "Devuelto"}
          >
            {data.estado === "Devuelto" ? "Devuelto" : "Hacer devolución"}
          </Button>
        );
      },
    },
  ];

  return (
    <Table
      scroll={{ x: "calc(100vh - 4em)" }}
      rowKey={(record) => record.display_id}
      onChange={(pagination) => {
        setpage(pagination.current);
        setlimit(pagination.pageSize);
      }}
      columns={columns}
      loading={loading}
      expandable={{
        expandedRowRender: (record) => {
          return (
            <Details
              data={record}
              refetching={refetch}
              SendPushNotificationNewordenUSer={
                SendPushNotificationNewordenUSer
              }
              actualizarOrderProcess={actualizarOrderProcess}
            />
          );
        },
        rowExpandable: (record) => record.display_id === record.display_id,
      }}
      dataSource={datos}
      pagination={{ defaultCurrent: 1, onChange: onChangePage, total: 1000 }}
    />
  );
}
