import React, { useState } from "react";
import Card from "./Card";
import "./index.css";
import { CheckCircleFilled, LoadingOutlined } from "@ant-design/icons";
import {
  DatePicker,
  Button,
  Modal,
  List,
  Avatar,
  message,
  Tag,
  Spin,
  Input,
  Select,
} from "antd";
import moment from "moment";
import { Query, useMutation } from "react-apollo";
import { mutation, query } from "../../GraphQL";
import { IMAGES_PATH, LOCAL_API_URL } from "../../Config";
//@ts-ignore
import locate from "moment/locale/es";
import dataMunicipio from "../Orders/municipio.json";

const { Search } = Input;
const { Option } = Select;

const { RangePicker } = DatePicker;

const date = new Date();
let day = date.getDate();
let month = date.getMonth() + 1;
let year = date.getFullYear();

const dateFormat = "MM/DD/YYYY";
const fromDate = `${month}/${day}/${year}`;
const toDate = "12/31/2021";

export default function Order() {
  const [actualizarOrderProcess] = useMutation(
    mutation.ACTUALIZAR_CUSTON_ORDER
  );
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [riderID, setriderID] = useState("");
  const [dataOrder, setdataOrder] = useState(null);
  const [search, setSearch] = useState();
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(20);
  const [dateRange, setDateRange] = useState(null);
  const [city, setcity] = useState(null);

  const messageRider = "🚨 🚨 Has recibido un nuevo PEDIDO EXPRESS 🚨 🚨";

  function onChange(value) {
    setcity(value);
  }

  const onDateRangeChange = (dates, dateStrings) => {
    const fromDate = dateStrings[0];
    const toDate = dateStrings[1];
    setDateRange({ fromDate: fromDate, toDate: toDate });
  };

  const aplicarDateRange = () => {
    setDateRange({ fromDate: fromDate, toDate: toDate });
  };

  const onSearch = (search) => {
    setSearch(Number(search));
    setpage(1);
  };

  let refetching;

  const openRider = (datos) => {
    setIsModalVisible(true);
    setdataOrder(datos);
    setriderID(datos.courier ? datos.courier : "");
  };

  const SendPushNotificationNeworden = (OnesignalID) => {
    fetch(
      `${LOCAL_API_URL}/send-push-notification-riders?IdOnesignal=${OnesignalID}&textmessage=${messageRider}`
    ).catch((err) => err);
  };

  const SendPushNotificationNewordenUSer = (
    OnesignalID,
    name,
    lastName,
    deliverect
  ) => {
    const messageUser = `Te hemos asignado a ${name} ${lastName} como repartidor para entregar tu pedido`;

    const messageUserDeliveret = `⭐️ 🌟 El pedido ha sido entregado es hora de decirno que te ha parecido`;

    fetch(
      `${LOCAL_API_URL}/send-push-notification?IdOnesignal=${
        deliverect ? messageUserDeliveret : OnesignalID
      }&textmessage=${messageUser}`
    ).catch((err) => err);
  };

  const asigRider = () => {
    const input = {
      id: dataOrder.id,
      riders: riderID,
      estado: "Asignada",
    };
    if (riderID) {
      actualizarOrderProcess({ variables: { input: input } }).then((res) => {
        if (res.data.actualizarOrderProcess.success) {
          refetching();
          setriderID("");
          setIsModalVisible(false);
          message.success("Orden actualizada");
          SendPushNotificationNeworden(dataOrder.Riders.OnesignalID);
          SendPushNotificationNewordenUSer(
            dataOrder.usuario.OnesignalID,
            dataOrder.Riders.name,
            dataOrder.Riders.lastName,
            false
          );
        }
      });
    } else {
      message.warning("Debes seleccionar un rider para continuar");
    }
  };

  const onChangePage = (page) => {
    setpage(page);
  };

  return (
    <div>
      <Query
        query={query.GET_CUSTOM_ORDER}
        variables={{
          city: city,
          orderID: search,
          dateRange: dateRange,
          limit: limit,
          page: page,
        }}
      >
        {(response) => {
          if (response) {
            response.refetch();
            refetching = response.refetch;
            const data =
              response && response.data && response.data.getCustomOrderAdmin
                ? response.data.getCustomOrderAdmin.data
                : [];
            return (
              <div>
                <div className="search_container">
                  <div>
                    <h3>Filtrar por fecha o ID</h3>
                    <RangePicker
                      locale={locate}
                      defaultValue={[
                        moment(fromDate, dateFormat),
                        moment(toDate, dateFormat),
                      ]}
                      format={dateFormat}
                      onChange={onDateRangeChange}
                      style={{ height: 35, marginRight: 10 }}
                    />
                    <Button
                      onClick={() => {
                        if (dateRange) {
                          setDateRange(null);
                        } else {
                          aplicarDateRange();
                        }
                      }}
                      style={{
                        marginBottom: 20,
                        marginTop: 10,
                        height: 35,
                      }}
                      type="dashed"
                    >
                      {dateRange ? "Cancelar" : "Aplicar"}
                    </Button>
                    <br />
                    <Select
                      showSearch
                      style={{ width: 300 }}
                      placeholder="Selectcciona una ciudad"
                      optionFilterProp="children"
                      allowClear
                      onChange={onChange}
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {dataMunicipio.map((data, i) => {
                        return (
                          <Option value={data.nombre} key={i}>
                            {data.nombre}
                          </Option>
                        );
                      })}
                    </Select>
                  </div>
                  <div>
                    <Search
                      style={{ width: "100%", minWidth: 350 }}
                      placeholder="Busca un pedido por ID"
                      allowClear
                      type="number"
                      enterButton="Buscar"
                      size="large"
                      onSearch={onSearch}
                      maxLength={6}
                    />
                  </div>
                </div>
                <Card
                  loading={response.loading}
                  datos={data}
                  setpage={setpage}
                  setlimit={setlimit}
                  openRider={openRider}
                  refetch={response.refetch}
                  riderID={riderID}
                  dataOrder={dataOrder}
                  setriderID={setriderID}
                  onChangePage={onChangePage}
                  SendPushNotificationNewordenUSer={
                    SendPushNotificationNewordenUSer
                  }
                />
              </div>
            );
          }
        }}
      </Query>
      {dataOrder ? (
        <Modal
          title={false}
          visible={isModalVisible}
          okText="Asignar"
          cancelText="Cancelar"
          onOk={() => asigRider()}
          okButtonProps={{ disabled: riderID ? false : true }}
          onCancel={() => {
            setIsModalVisible(false);
            setriderID(null);
          }}
        >
          <div className="modal_contents">
            <Query query={query.GET_RIDER} variables={{ city: dataOrder.city }}>
              {(response) => {
                if (response.loading) {
                  return (
                    <div className="modal_contents_loading">
                      <Spin
                        indicator={
                          <LoadingOutlined style={{ fontSize: 34 }} spin />
                        }
                      />
                    </div>
                  );
                }
                if (response) {
                  const riders =
                    response && response.data && response.data.getRiderForAdmin
                      ? response.data.getRiderForAdmin.data
                      : [];

                  riders.sort((a, b) =>
                    a.numberCurrentOrder > b.numberCurrentOrder
                      ? 1
                      : b.numberCurrentOrder > a.numberCurrentOrder
                      ? -1
                      : 0
                  );

                  response.refetch();

                  return (
                    <List
                      style={{ paddingTop: 50, paddingBottom: 50 }}
                      itemLayout="horizontal"
                      dataSource={riders}
                      renderItem={(item) => (
                        <List.Item
                          onClick={() => setriderID(item._id)}
                          style={{ cursor: "pointer" }}
                        >
                          <List.Item.Meta
                            avatar={
                              <Avatar
                                src={IMAGES_PATH + item.avatar}
                                size={50}
                              />
                            }
                            title={`${item.name} ${item.lastName}`}
                            description={
                              <div>
                                {item.numberCurrentOrder === 0 ? (
                                  <Tag color="#87d068">
                                    Disponible sin pedidos asignado
                                  </Tag>
                                ) : (
                                  <>
                                    {item.numberCurrentOrder > 3 ? (
                                      <Tag color="#cd201f">
                                        Muchos pedidos asignado: más de{" "}
                                        {item.numberCurrentOrder} pedidos
                                      </Tag>
                                    ) : (
                                      <Tag color="orange">
                                        Tiene asignada ahora:{" "}
                                        {item.numberCurrentOrder}{" "}
                                        {item.numberCurrentOrder > 1
                                          ? "pedidos"
                                          : "pedido"}
                                      </Tag>
                                    )}
                                  </>
                                )}
                              </div>
                            }
                          />
                          {riderID === item._id ? (
                            <CheckCircleFilled
                              style={{ color: "#90c33c", fontSize: 18 }}
                            />
                          ) : null}
                        </List.Item>
                      )}
                    />
                  );
                }
              }}
            </Query>
          </div>
        </Modal>
      ) : null}
    </div>
  );
}
