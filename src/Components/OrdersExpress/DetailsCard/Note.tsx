import React from "react";
import "./index.css";

export default function Note(props: any) {
  const { title, data } = props;
  return (
    <div className="items-cart" style={{ marginLeft: 20, width: "100%" }}>
      <h3>{title}</h3>
      <p>{data.nota}</p>
    </div>
  );
}
