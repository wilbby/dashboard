import React from "react";
import "./index.css";
import { formaterPrice } from "../../../Utils/formatePrice";
import { Button } from "antd";

export default function Items(props: any) {
  const { data, title } = props;

  var totals = Number(data.total);
  var stimate = Number(data.product_stimate_price);

  var tot = totals;
  var stimateTotal = stimate;

  return (
    <div className="items-cart">
      <h3>{title}</h3>

      <div className="items-cart-resumen">
        <div>
          <ul>
            <li>
              <p>Envio:</p>
            </li>
            {data.product_stimate_price ? (
              <li>
                <p>Coste estimado:</p>
              </li>
            ) : null}
            <li>
              <p>Propina:</p>
            </li>
            <li>
              <h1>Total:</h1>
            </li>
          </ul>
        </div>

        <div>
          <ul>
            <li>
              <p>{formaterPrice(tot / 100, "es-ES", "EUR")}</p>
            </li>
            {data.product_stimate_price ? (
              <li>
                <p>{formaterPrice(stimateTotal / 100, "es-ES", "EUR")}</p>
              </li>
            ) : null}

            <li>
              <p>{formaterPrice(0 / 100, "es-ES", "EUR")}</p>
            </li>
            <li>
              <h1>{formaterPrice(tot / 100, "es-ES", "EUR")}</h1>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
