import React from "react";
import "./index.css";
import { Timeline } from "antd";
import moment from "moment";

export default function Items(props: any) {
  const { data, title } = props;
  const status = data.statusProcess ? data.statusProcess : [];
  return (
    <div className="items-cart">
      <h3>{title}</h3>
      <Timeline style={{ marginTop: 30 }}>
        {status.map((datos: any, i: number) => {
          return (
            <Timeline.Item key={i}>
              {moment(datos.date).format("HH:mm")} - {datos.status}
            </Timeline.Item>
          );
        })}
      </Timeline>
    </div>
  );
}
