import React from "react";
import "./index.css";
import Items from "./Items";
import NoteOrder from "./Note";
import Resumen from "./Resumen";
import PaymentType from "./PaymentType";
import Hora from "./Hora";
import Distance from "./Distance";
import { Button, message } from "antd";

export default function DetailsCard(props: any) {
  const {
    data,
    refetching,
    SendPushNotificationNewordenUSer,
    actualizarOrderProcess,
  } = props;

  const DeliveretOrder = () => {
    const input = {
      id: data.id,
      estado: "Entregado",
    };
    actualizarOrderProcess({ variables: { input: input } }).then((res: any) => {
      console.log("entregado", res);
      if (res.data.actualizarOrderProcess.success) {
        refetching();
        message.success("El pedido ha sido marcado como entregado");
        SendPushNotificationNewordenUSer(
          data.usuario.OnesignalID,
          data.Riders.name,
          data.Riders.lastName,
          true
        );
      }
    });
  };

  return (
    <div className="new-menu-cart-container">
      <div className="new-cart-wrapper">
        <div className="new-cart-content">
          <div className="carts">
            {data.fronStore ? (
              <Items
                title="Info del cliente"
                title2={`${data.customer.name}`}
                sub={data.customer.phone}
              />
            ) : (
              <Items
                title="Info del cliente"
                title2={`${data.usuario.name} ${data.usuario.lastName}`}
                sub={data.usuario.telefono}
                sub1={data.usuario.email}
              />
            )}

            {data.riders ? (
              <Items
                title="Info del repartidor"
                title2={`${data.Riders.name} ${data.Riders.lastName}`}
                sub={data.Riders.telefono}
                sub1={data.Riders.email}
              />
            ) : null}

            <Items
              title="Dirección de recogida"
              p={`${data.fronStore ? `${data.Store.title} - ` : null}  ${
                data.origin.address_name
              },
                ${data.origin.address_number},
                ${
                  data.origin.postcode ? data.origin.postcode : "No postalcode"
                },
                ${data.city}, ${data.origin.type}`}
            />

            <Items
              title="Dirección de entrega"
              p={`${data.destination.address_name},
                ${data.destination.address_number},
                ${
                  data.destination.postcode
                    ? data.destination.postcode
                    : "No postalcode"
                },
                ${data.city}, ${data.destination.type}`}
            />
          </div>
          <div
            className="carts"
            style={{ width: "55%", marginRight: 40, minWidth: 500 }}
          >
            <NoteOrder title="Nota del pedido" data={data} />
          </div>
          <div className="carts">
            <Hora title="Hora de entrega" data={data} />

            <Distance title="Distancia" data={data} />
            <Resumen title="Resumen" data={data} />
            <PaymentType title="Método de pago" data={data} />
            {data.estado !== "Devuelto" ? (
              <>
                {data.estado !== "Entregado" ? (
                  <div style={{ marginTop: 20, marginBottom: 20 }}>
                    <Button
                      type="primary"
                      style={{ width: "100%", height: 40 }}
                      onClick={() => DeliveretOrder()}
                    >
                      MARCAR COMO ENTREGADO
                    </Button>
                  </div>
                ) : null}
              </>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}
