import React from "react";
import "./index.css";

export default function Items(props: any) {
  const { data, title } = props;

  const displayDistance = Number(data.distance);
  const dist = displayDistance;

  return (
    <div className="items-cart">
      <div className="items-cart-horario">
        <h3>{title}</h3>
        <h1 style={{ fontWeight: "bold", color: "#90c33c" }}>
          {dist.toFixed(1)} km
        </h1>
      </div>
    </div>
  );
}
