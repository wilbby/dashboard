import styles from "./style.module.css";
import { List } from "antd";
import { formaterPrice } from "../../../Utils/formatePrice";

export default function Product(props: any) {
  const { data, loading, language, currency } = props;

  const renderItems = (item: any) => {
    return (
      <div className="info_product">
        {item.items &&
          item.items.subItems.map((sub: any, i: number) => {
            const subs = sub ? sub.subItems : [];
            return (
              <div key={i} style={{ marginTop: 5 }}>
                <p>
                  <span style={{ color: "#90c33c" }}>
                    {sub.subItems && sub.subItems.length > 0
                      ? sub.quantity
                      : sub.quantity > 1
                      ? `${sub.quantity} x`
                      : "-"}{" "}
                  </span>{" "}
                  {sub.name}{" "}
                  <span style={{ color: "#90c33c" }}>
                    {formaterPrice(sub.price / 100, language, currency)}
                  </span>
                </p>
                {subs &&
                  subs.map((s: any, i: number) => {
                    return (
                      <p key={i}>
                        <span style={{ color: "#90c33c" }}>{s.quantity} x</span>{" "}
                        {s.name}{" "}
                        <span style={{ color: "#90c33c" }}>
                          {formaterPrice(sub.price / 100, language, currency)}
                        </span>
                      </p>
                    );
                  })}
              </div>
            );
          })}
      </div>
    );
  };
  return (
    <div className="addres_info_container">
      <List
        loading={loading}
        itemLayout="horizontal"
        dataSource={data.items}
        renderItem={(item) => renderItems(item)}
      />
    </div>
  );
}
