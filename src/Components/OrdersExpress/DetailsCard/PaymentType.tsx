import React from "react";
import "./index.css";

export default function Items(props: any) {
  const { data, title } = props;

  const img = data.pagoPaypal
    ? "https://internal-image-wilbby.s3.eu-west-3.amazonaws.com/paypal.png"
    : "https://internal-image-wilbby.s3.eu-west-3.amazonaws.com/targetas.png";

  return (
    <div className="items-cart">
      <h3>{title}</h3>

      <div className="items-cart-paymentMethod">
        <div>
          <img src={img} alt={data.paymentMethod} />
        </div>
        <div>
          <p>{data.paymentMethod}</p>
        </div>
      </div>
    </div>
  );
}
