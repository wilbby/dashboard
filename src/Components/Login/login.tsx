import React, { useState } from "react";
import Logo from "../../Assets/image/logowhite.png";
import { message, Spin } from "antd";
import { useMutation } from "react-apollo";
import { mutation } from "../../GraphQL";
import { Link, withRouter } from "react-router-dom";
import "./index.css";

function LoginComponent(props: any) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [Loading, setLoading] = useState(false);

  const { history } = props;

  const [autenticarAdmin] = useMutation(mutation.AUTENTICAR_ADMIN);

  const OnLogin = (event: any) => {
    setLoading(true);
    event.preventDefault();
    autenticarAdmin({
      variables: {
        email,
        password,
      },
    })
      .then((res) => {
        setLoading(false);
        const datos = res.data.autenticarAdmin;
        if (datos.success) {
          message.success(datos.message);
          localStorage.setItem("id", datos.data.id);
          localStorage.setItem("token", datos.data.token);
        } else {
          message.error(datos.message);
        }
      })
      .catch((e) => {
        setLoading(false);
        console.log(e);
        message.error("Algo va mal intentalo de nuevo por favor");
      })
      .finally(() => {
        setLoading(false);
        history.push(`/home`);
      });
  };

  const handleChangeEmail = (event: any) => {
    setEmail(event.target.value);
  };

  const handleChangePassword = (event: any) => {
    setPassword(event.target.value);
  };

  return (
    <div className="Login_Container">
      <img src={Logo} alt="Wilbby" />
      <div className="Login_Box">
        {Loading ? (
          <Spin size="default" />
        ) : (
          <form className="Box" onSubmit={OnLogin}>
            <input
              name="email"
              type="email"
              placeholder="Correo Electrónico"
              required={true}
              onChange={handleChangeEmail}
            />
            <input
              name="password"
              type="password"
              placeholder="Contraseña"
              required={true}
              onChange={handleChangePassword}
            />
            <button type="submit" value="Submit">
              Iniciar sesión
            </button>
            <Link to="/">¿Olvidate tu contraseña?</Link>
          </form>
        )}
      </div>
    </div>
  );
}

export default withRouter(LoginComponent);
