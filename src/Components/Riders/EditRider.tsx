import React, { useState } from "react";
import { Modal } from "antd";
import { Button, Select, message } from "antd";
import dataMunicipio from "../Orders/municipio.json";
import { useMutation } from "react-apollo";
import { mutation } from "../../GraphQL";

const { Option } = Select;

export default function AddRiders(props: any) {
  const { visible, setVisible, refetch, data, setDataRider } = props;

  const [city, setcity] = useState(data.city);

  const [actualizarRiders] = useMutation(mutation.ACTUALIZAR_RIDER);

  const handleCancel = () => {
    setDataRider(null);
    setVisible(false);
  };

  function onChange(value: string) {
    setcity(value);
  }

  const onFinish = () => {
    const input = {
      _id: data._id,
      city: city,
    };

    actualizarRiders({ variables: { input: input } })
      .then((res: any) => {
        const result = res.data.actualizarRiders;
        if (result._id) {
          message.success("Repartidor actualizado con exito");
          refetch();
        } else {
          message.warning("Algo salio mal inentalo de nuevo");
        }
      })
      .catch((e) => {
        console.log(e);
        message.error("Algo salio mal");
      });
  };

  return (
    <Modal visible={visible} onCancel={handleCancel} footer={null} width={400}>
      <div style={{ padding: 20 }}>
        <Select
          showSearch
          value={city}
          style={{ width: "100%", marginTop: 50 }}
          placeholder="Selectcciona una ciudad"
          allowClear
          onChange={onChange}
          //@ts-ignore
          autoComplete="none"
          filterOption={(input, option) =>
            //@ts-ignore
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {dataMunicipio.map((data, i) => {
            return (
              <Option value={data.nombre} key={i}>
                {data.nombre}
              </Option>
            );
          })}
        </Select>

        <br />

        <Button
          onClick={onFinish}
          type="primary"
          htmlType="submit"
          style={{ width: "100%", marginTop: 20 }}
        >
          Añadir repartidor
        </Button>
      </div>
    </Modal>
  );
}
