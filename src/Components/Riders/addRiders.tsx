import React from "react";
import { Modal } from "antd";
import { Form, Input, Button, Select, message } from "antd";
import dataMunicipio from "../Orders/municipio.json";
import { useMutation } from "react-apollo";
import { mutation } from "../../GraphQL";

const { Option } = Select;

export default function AddRiders(props: any) {
  const { visible, setVisible, sendActivation, refetch } = props;

  const [crearRiders] = useMutation(mutation.CREATE_RIDER);

  const handleCancel = () => {
    setVisible(false);
  };

  const onFinish = (values: any) => {
    values.password = "admin";
    crearRiders({ variables: { input: values } })
      .then((res) => {
        const result = res.data.crearRiders;
        if (result.success) {
          sendActivation(values.email);
          message.success("Repartidor añadido con exito");
          refetch();
        } else {
          message.warning("Algo salio mal inentalo de nuevo");
        }
      })
      .catch(() => {
        message.error("Algo salio mal");
      });
  };

  const validateMessages = {
    required: "Este campo es requerido!",
    types: {
      email: "Este email no es válido!",
    },
  };

  return (
    <Modal visible={visible} onCancel={handleCancel} footer={null} width={400}>
      <div style={{ padding: 20 }}>
        <Form
          name="nest-messages"
          onFinish={onFinish}
          validateMessages={validateMessages}
          style={{ marginTop: 50 }}
        >
          <Form.Item name="nombre" rules={[{ required: true }]}>
            <Input placeholder="Nombre" />
          </Form.Item>
          <Form.Item name="apellidos" rules={[{ required: true }]}>
            <Input placeholder="Apellidos" />
          </Form.Item>
          <Form.Item name="email" rules={[{ type: "email", required: true }]}>
            <Input placeholder="Correo electrónico" />
          </Form.Item>

          <Form.Item name="telefono" rules={[{ required: true }]}>
            <Input placeholder="Teléfono" />
          </Form.Item>

          <Form.Item rules={[{ required: true }]} name="city">
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Selectcciona una ciudad"
              allowClear
              //@ts-ignore
              autoComplete="none"
              filterOption={(input, option) =>
                //@ts-ignore
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {dataMunicipio.map((data, i) => {
                return (
                  <Option value={data.nombre} key={i}>
                    {data.nombre}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Añadir repartidor
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Modal>
  );
}
