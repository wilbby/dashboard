import React, { useState } from "react";
import { Button, message, List, Avatar, Skeleton, Switch, Modal } from "antd";
import {
  PlusCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { LOCAL_API_URL } from "../../Config";
import { useQuery, useMutation } from "react-apollo";
import { query, mutation } from "../../GraphQL";
import AddRiders from "./addRiders";
import EditRiders from "./EditRider";

const { confirm } = Modal;

export default function Riders() {
  const [itemID, setitemID] = useState(null);
  const [Loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [dataRider, setDataRider] = useState(null);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [actualizarRiders] = useMutation(mutation.ACTUALIZAR_RIDER);
  const [eliminarRiders] = useMutation(mutation.DELETED_RIDER);

  const { data, loading, refetch } = useQuery(query.GET_RIDER_ALL);

  const riders =
    data && data.getRiderForAdminAll ? data.getRiderForAdminAll.data : [];

  const sendActivation = (email: string) => {
    if (email) {
      fetch(`${LOCAL_API_URL}/send-email-welcome-riders?email=${email}`).then(
        async (res) => {
          const data = await res.json();
          if (data.success) {
            message.success("Enlace enviado con exito");
          } else {
            message.error("Algo salio mal inentalo de nuevo");
          }
        }
      );
    } else {
      message.warning("Debes añadir un email para continuar");
    }
  };

  const deletedRidersList = (id: string) => {
    eliminarRiders({ variables: { id: id } })
      .then((res) => {
        const result = res.data.eliminarRiders;
        if (result.success) {
          message.success(result.messages);
          if (refetch) {
            refetch();
          }
        } else {
          message.success(result.messages);
        }
      })
      .catch(() => {
        message.error("Algo salio mal");
      });
  };

  const detetedRider = (id: string) => {
    confirm({
      title: "¿Estás seguro que deseas eliminar este repartidos?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar un repartidor perderemos los datos almacenado del mismo",
      cancelText: "Cancelar",
      okText: "Eliminar",
      onOk() {
        deletedRidersList(id);
      },
      onCancel() {},
    });
  };

  const setOnlineRider = (id: any, cheket: boolean) => {
    setitemID(id);
    setLoading(true);
    const input = {
      _id: id,
      isAvalible: cheket,
    };

    actualizarRiders({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarRiders._id) {
          setLoading(false);
          setitemID(null);
          message.success("Todo ha salido bien");
          refetch();
        } else {
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch(() => {
        setLoading(false);
        message.error("Algo salio mal intentalo de nuevo");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div>
      {loading ? (
        <>
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
        </>
      ) : (
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"
          dataSource={riders}
          header={
            <div>
              <div className="search_container">
                <Button
                  type="primary"
                  style={{ marginTop: 20, height: 45 }}
                  icon={<PlusCircleOutlined />}
                  onClick={() => setVisible(true)}
                >
                  Crear nuevo repartidor
                </Button>
              </div>
            </div>
          }
          renderItem={(item: any) => (
            <List.Item
              actions={[
                <Button
                  type="link"
                  onClick={() => {
                    setVisibleEdit(true);
                    setDataRider(item);
                  }}
                >
                  Cambiar ciudad
                </Button>,
                <Button
                  type="link"
                  danger
                  onClick={() => detetedRider(item._id)}
                >
                  Eliminar
                </Button>,
              ]}
            >
              <List.Item.Meta
                avatar={
                  <Avatar
                    size={50}
                    src={`https://api.v2.wilbby.com/assets/images/${item.avatar}`}
                  />
                }
                title={
                  <h3>
                    {item.name} {item.lastName}
                  </h3>
                }
                description={
                  <>
                    {item.city} <br /> {item.telefono} <br /> {item.email}
                  </>
                }
              />
              <div>
                <Switch
                  checked={item.isAvalible}
                  loading={Loading && itemID === item._id}
                  onChange={(value) => {
                    setOnlineRider(item._id, value);
                  }}
                  checkedChildren="Disponible"
                  unCheckedChildren="No disponible"
                />
              </div>
            </List.Item>
          )}
        />
      )}
      <AddRiders
        visible={visible}
        setVisible={setVisible}
        sendActivation={sendActivation}
        refetch={refetch}
      />
      {dataRider ? (
        <EditRiders
          data={dataRider}
          visible={visibleEdit}
          setVisible={setVisibleEdit}
          refetch={refetch}
          setDataRider={setDataRider}
        />
      ) : null}
    </div>
  );
}
