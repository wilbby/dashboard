import { useState } from "react";
import {
  Modal,
  Input,
  Select,
  message,
  Upload,
  Tooltip,
  Button,
  DatePicker,
  Switch,
} from "antd";
import { mutation } from "../../GraphQL";
import { useMutation } from "react-apollo";
import { PlusCircleOutlined, LoadingOutlined } from "@ant-design/icons";
import dataMunicipio from "../Orders/municipio.json";
import moment from "moment";

const { Option } = Select;

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function ModalEditAdd({
  visible,
  setVisible,
  refetch,
  data,
  setDataads,
}: any) {
  const [imagen, setImagen] = useState(data ? data.image : "");
  const [title, setTitle] = useState(data ? data.name : "");
  const [includeCity, setIncludeCity] = useState(data ? data.includeCity : []);
  const [url, setUrl] = useState(data ? data.url : "");
  const [endDate, setEndDate] = useState(data ? data.end_date : null);
  const [email, setEmail] = useState(data ? data.email : "");
  const [subject, setsubject] = useState(
    data && data.dataEmail ? data.dataEmail.subject : ""
  );
  const [body, setbody] = useState(
    data && data.dataEmail ? data.dataEmail.body : ""
  );
  const [navigate, setnavigate] = useState(data ? data.navigate : true);
  const [loadingImage, setloadingImage] = useState(false);
  const [loading, setLoading] = useState(false);

  const [singleUploadToStoreImagenAws] = useMutation(
    mutation.UPLOAD_FILE_STORE
  );

  const [createAds] = useMutation(mutation.CREATED_ADS);
  const [updateAds] = useMutation(mutation.UPDATE_ADS);

  function onChange(value: any) {
    setIncludeCity(value);
  }

  function onChangeDate(date: any, dateString: any) {
    setEndDate(moment(dateString).format());
  }

  const isOK = () => {
    if (imagen) {
      return false;
    } else {
      return true;
    }
  };

  const saveAds = () => {
    setLoading(true);

    const dataEmail = {
      subject: subject,
      body: body,
    };
    const input = {
      name: title,
      image: imagen,
      sorting: 1,
      visible: true,
      navigate: navigate,
      includeCity: includeCity,
      url: navigate ? url : null,
      email: email ? email : null,
      category: "5fb7aec4b234a46c0929780b",
      isHome: false,
      dataEmail: email ? dataEmail : null,
      click: 1,
      end_date: endDate,
    };

    createAds({ variables: { data: { data: input } } })
      .then((res) => {
        if (res.data.createAds.success) {
          refetch();
          message.success("Anuncio creado con exito");
          setVisible(false);
          setLoading(false);
        } else {
          refetch();
          setLoading(false);
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const updateAdss = () => {
    setLoading(true);

    const dataEmail = {
      subject: subject,
      body: body,
    };
    const input = {
      _id: data._id,
      name: title,
      image: imagen,
      sorting: data.sorting,
      visible: data.visible,
      navigate: navigate,
      includeCity: includeCity,
      url: navigate ? url : null,
      email: email ? email : null,
      category: data.category,
      isHome: data.isHome,
      dataEmail: email ? dataEmail : null,
      click: data.click,
      end_date: endDate,
    };

    updateAds({ variables: { data: input } })
      .then((res) => {
        if (res.data.updateAds.success) {
          refetch();
          message.success("Anuncio actualizado con exito");
          setVisible(false);
          setLoading(false);
        } else {
          refetch();
          setLoading(false);
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const uploadButton = (
    <Tooltip title="Añadir foto de banner">
      <div
        style={{
          width: 450,
          height: 150,
          borderRadius: 15,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadingImage ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        )}

        <div className="ant-upload-text">
          <span style={{ fontSize: 12 }}>Añadir foto de banner</span>
        </div>
      </div>
    </Tooltip>
  );

  return (
    <Modal
      visible={visible}
      onCancel={() => {
        setVisible(false);
        setDataads(null);
      }}
      width={500}
      onOk={() => {
        if (data) {
          updateAdss();
        } else {
          saveAds();
        }
      }}
      okButtonProps={{ disabled: isOK(), loading: loading }}
      okText="Guardar cambios"
      cancelText="Cancelar"
    >
      <div>
        <div
          style={{
            width: 450,
            height: 200,
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: 30,
          }}
        >
          <Upload
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            customRequest={async (data) => {
              setloadingImage(true);
              let file = await getBase64(data.file);
              singleUploadToStoreImagenAws({
                variables: { file },
              })
                .then((res: any) => {
                  setloadingImage(false);
                  setImagen(
                    res.data.singleUploadToStoreImagenAws.data.Location
                  );
                })
                .catch((error: any) => {
                  setloadingImage(false);
                  message.error(
                    "Imagen muy grande reduce el tamaño de la misma"
                  );
                });
            }}
          >
            {imagen ? (
              <Tooltip title="Haz click para cambiar">
                <img className="imagen_prod_add" src={imagen} />
              </Tooltip>
            ) : null}

            {!imagen ? uploadButton : null}
          </Upload>

          <div
            style={{
              textAlign: "center",
              marginTop: 10,
            }}
          >
            <Button
              type="primary"
              href="https://www.iloveimg.com/es/comprimir-imagen"
              target="_blank"
            >
              Comprimir imagen
            </Button>
          </div>
        </div>
        <div style={{ marginTop: 30, marginBottom: 20 }}>
          <Input
            placeholder="Nombre del banner"
            defaultValue={title}
            onChange={(value: any) => setTitle(value.target.value)}
          />

          <div style={{ marginTop: 15, marginBottom: 15 }}>
            <h2>Destino del anuncio</h2>

            <Switch
              checkedChildren="URL"
              unCheckedChildren="Email"
              defaultChecked={navigate}
              onChange={(checked: boolean) => setnavigate(checked)}
            />

            {navigate ? (
              <Input
                placeholder="URL del anuncio"
                defaultValue={url}
                style={{ marginTop: 10 }}
                onChange={(value: any) => setUrl(value.target.value)}
              />
            ) : (
              <div>
                <Input
                  placeholder="Correo electrónico"
                  defaultValue={email}
                  style={{ marginTop: 10 }}
                  onChange={(value: any) => setEmail(value.target.value)}
                />
                <Input
                  placeholder="Asunto del email"
                  defaultValue={subject}
                  style={{ marginTop: 10 }}
                  onChange={(value: any) => setsubject(value.target.value)}
                />
                <Input
                  placeholder="Mensage predeterminado"
                  defaultValue={body}
                  style={{ marginTop: 10 }}
                  onChange={(value: any) => setbody(value.target.value)}
                />
              </div>
            )}
          </div>

          <Select
            mode="multiple"
            allowClear
            style={{
              width: "100%",
              marginTop: 15,
            }}
            placeholder="Selecciona uno o varias ciudades"
            onChange={onChange}
            value={includeCity}
          >
            {dataMunicipio.map((data: any, i: number) => {
              return (
                <Option key={i} value={data.nombre}>
                  {data.nombre}
                </Option>
              );
            })}
          </Select>

          <DatePicker
            onChange={onChangeDate}
            style={{ marginTop: 15, width: "100%" }}
            placeholder="Seleccionar una fecha de finalización"
            locale={{
              lang: {
                locale: "es_ES",
                placeholder: "Seleccionar fecha",
                rangePlaceholder: ["Start date", "End date"],
                today: "Hoy",
                now: "Ahora",
                backToToday: "Back to today",
                ok: "OK",
                clear: "Clear",
                month: "Month",
                year: "Year",
                timeSelect: "Seleccionar hora",
                dateSelect: "Seleccionar fecha",
                monthSelect: "Choose a month",
                yearSelect: "Choose a year",
                decadeSelect: "Choose a decade",
                yearFormat: "YYYY",
                dateFormat: "M/D/YYYY",
                dayFormat: "D",
                dateTimeFormat: "M/D/YYYY HH:mm:ss",
                monthFormat: "MMMM",
                monthBeforeYear: true,
                previousMonth: "Previous month (PageUp)",
                nextMonth: "Next month (PageDown)",
                previousYear: "Last year (Control + left)",
                nextYear: "Next year (Control + right)",
                previousDecade: "Last decade",
                nextDecade: "Next decade",
                previousCentury: "Last century",
                nextCentury: "Next century",
              },
              timePickerLocale: {
                placeholder: "Select time",
              },
              dateFormat: "YYYY-MM-DD",
              dateTimeFormat: "YYYY-MM-DD HH:mm:ss",
              weekFormat: "YYYY-wo",
              monthFormat: "YYYY-MM",
            }}
          />
          {data ? (
            <p style={{ marginTop: 5 }}>{moment(endDate).format("LL")}</p>
          ) : null}
        </div>
      </div>
    </Modal>
  );
}
