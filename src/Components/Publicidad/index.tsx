import { useState } from "react";
import {
  Table,
  Tag,
  Space,
  Button,
  Popover,
  Select,
  message,
  Popconfirm,
} from "antd";
import "./index.css";
import { PlusCircleOutlined, EyeOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "react-apollo";
import { query, mutation } from "../../GraphQL";
import moment from "moment";
import dataMunicipio from "../Orders/municipio.json";
import ModalAddEdit from "./ModalEditAdd";

const { Option } = Select;

const content = (
  <div>
    <p>Click de usuario en el anuncio</p>
  </div>
);

export default function Publicidad() {
  const [city, setcity] = useState("Tomelloso");
  const [visible, setVisible] = useState(false);
  const [visibleAdd, setVisibleAdd] = useState(false);
  const [dataAds, setDataads] = useState(null);

  const [eliminarAds] = useMutation(mutation.DELETED_ADS);
  const { data, refetch, loading } = useQuery(query.GET_RANDOM_ADS, {
    variables: { city: city },
  });
  const ads = data && data.getAds ? data.getAds.data : [];

  const openModal = (datas: any) => {
    setDataads(datas);
    setVisible(true);
  };

  const deletedAds = (id: string) => {
    eliminarAds({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarAds.success) {
          refetch();
          message.success("Anuncio eliminado con exito");
          setDataads(null);
        } else {
          refetch();
          message.warning("Algo salio mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        console.log(e);
        message.error("Algo salio mal intentalo de nuevo");
      });
  };

  const columns = [
    {
      title: "Imagen",
      dataIndex: "image",
      key: "image",
      render: (image: any) => (
        <div className="imagen">
          <img src={image} alt="Ads Wilbby" />
        </div>
      ),
    },
    {
      title: "Nombre",
      dataIndex: "name",
      key: "name",
      render: (text: any) => <p>{text}</p>,
    },
    {
      title: "Fecha inicio",
      key: "created_at",
      render: (datas: any) => (
        <p>
          {moment(datas.created_at)
            .utcOffset(60)
            .add(45, "minute")
            .format("lll")}
        </p>
      ),
    },
    {
      title: "Fecha de fin",
      key: "end_date",
      render: (datas: any) => (
        <p>
          {moment(datas.end_date)
            .utcOffset(60)
            .add(45, "minute")
            .format("lll")}
        </p>
      ),
    },
    {
      title: "Ciudades",
      dataIndex: "includeCity",
      render: (tags: any) => (
        <span>
          {tags.map((tag: any) => {
            let color = tag.length > 5 ? "geekblue" : "green";
            if (tag === "loser") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag} style={{ margin: 5 }}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </span>
      ),
    },
    {
      title: "Clicks",
      dataIndex: "click",
      render: (tags: any) => (
        <Popover content={content} title="Clicks del anuncio">
          <span>
            <EyeOutlined /> {tags}
          </span>
        </Popover>
      ),
    },
    {
      title: "Acción",
      key: "action",
      render: (data: any) => (
        <Space size="middle">
          <Button type="link" onClick={() => openModal(data)}>
            Editar
          </Button>
          <Popconfirm
            title="¿Estas seguro que deseas eliminar este anuncio?"
            onConfirm={() => deletedAds(data._id)}
            onCancel={() => {}}
            okText="Eliminar"
            cancelText="Cancelar"
          >
            <Button type="link" danger>
              Eliminar
            </Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: 40,
          marginTop: 20,
        }}
      >
        <Select
          defaultValue={city}
          showSearch
          style={{ width: 300 }}
          placeholder="Selectcciona una ciudad"
          optionFilterProp="children"
          allowClear
          onChange={(value: any) => setcity(value)}
          filterOption={(input: any, option: any) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {dataMunicipio.map((data, i) => {
            return (
              <Option value={data.nombre} key={i}>
                {data.nombre}
              </Option>
            );
          })}
        </Select>

        <Button
          icon={<PlusCircleOutlined />}
          type="primary"
          onClick={() => setVisibleAdd(true)}
        >
          Añadir banner
        </Button>
      </div>
      <Table columns={columns} dataSource={ads} loading={loading} />
      {dataAds ? (
        <ModalAddEdit
          visible={visible}
          setVisible={setVisible}
          refetch={refetch}
          data={dataAds}
          setDataads={setDataads}
        />
      ) : null}

      <ModalAddEdit
        visible={visibleAdd}
        setVisible={setVisibleAdd}
        refetch={refetch}
        data={null}
        setDataads={setDataads}
      />
    </div>
  );
}
