import React, { useState } from "react";
import Card from "./Card";
import "./index.css";
import { CheckCircleFilled, LoadingOutlined } from "@ant-design/icons";
import {
  DatePicker,
  Button,
  Modal,
  List,
  Avatar,
  message,
  Tag,
  Spin,
  Input,
  Select,
  Popover,
  Statistic,
} from "antd";
import moment from "moment";
import { Query, useMutation, useQuery } from "react-apollo";
import { mutation, query } from "../../GraphQL";
import { IMAGES_PATH } from "../../Config";
//@ts-ignore
import locate from "moment/locale/es";
import dataMunicipio from "./municipio.json";
import Report from "./Report";
import { RiderOrder } from "./OrderRider";

const { Search } = Input;
const { Option } = Select;
const { RangePicker } = DatePicker;

const date = new Date();
let day = date.getDate();
let month = date.getMonth() + 1;
let year = date.getFullYear();

let day1 = date.getDate();
let month1 = date.getMonth() + 2;
let year1 = date.getFullYear();

const dateFormat = "MM/DD/YYYY";
const fromDate = `${month}/${day}/${year}`;
const toDate = `${month1}/${day1}/${year1}`;

export default function Order({ citys }) {
  const [NewOrdenAsigRider] = useMutation(mutation.ASING_RIDERS);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalReportVisible, setIsModalReportVisible] = useState(false);
  const [riderID, setriderID] = useState("");
  const [dataOrder, setdataOrder] = useState(null);
  const [search, setSearch] = useState();
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(30);
  const [dateRange, setDateRange] = useState(null);
  const [city, setcity] = useState(citys);
  const [stores, setstores] = useState([]);
  const [rider, setrider] = useState([]);
  const [searchStore, setsearchStore] = useState("");
  const [tipo, setTipo] = useState("Periodo");

  const { data, loading, refetch } = useQuery(query.GET_ORDER, {
    variables: {
      page: page,
      limit: limit,
      orderID: search,
      dateRange: {
        fromDate: dateRange ? dateRange.fromDate : null,
        toDate: dateRange ? dateRange.toDate : null,
      },
      status: [
        "Nueva",
        "Confirmada",
        "En la cocina",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
        "Entregada",
        "Rechazado",
        "Rechazada por la tienda",
        "Rechazada por el rider",
        "Devuelto",
        "Finalizada",
        "Resolución",
      ],
      Stores: stores.length > 0 ? stores : null,
      city: city,
      rider: rider,
    },
  });

  const orders =
    data && data.getNewOrderAdimin ? data.getNewOrderAdimin.list : [];

  const count =
    data && data.getNewOrderAdimin ? data.getNewOrderAdimin.count : 0;

  setInterval(()=> {
    if (refetch) {
      refetch();
    }
  }, 5000)

  function handleChangeTipo(value) {
    setTipo(value);
  }

  function handleChange(value) {
    setstores(value);
  }

  function onChange(value) {
    setcity(value);
  }

  function onSearchStore(search) {
    setsearchStore(search);
  }

  function onChangeRiders(value) {
    setrider(value);
  }

  const onDateRangeChange = (dates, dateStrings) => {
    const fromDate = dateStrings[0];
    const toDate = dateStrings[1];
    setDateRange({
      fromDate:
        tipo === "Sólo un día" ? moment(dates).format(dateFormat) : fromDate,
      toDate: tipo === "Sólo un día" ? null : toDate,
    });
  };

  const onSearch = (search) => {
    setSearch(Number(search));
    setpage(1);
  };

  const openRider = (datos) => {
    setIsModalVisible(true);
    setdataOrder(datos);
    setriderID(datos.courier ? datos.courier : "");
  };

  const asigRider = () => {
    if (riderID) {
      NewOrdenAsigRider({
        variables: { ordenId: dataOrder._id, riderID: riderID },
      })
        .then((res) => {
          if (res.data.NewOrdenAsigRider.success) {
            refetch();
            setriderID("");
            setIsModalVisible(false);
            message.success(res.data.NewOrdenAsigRider.messages);
          } else {
            refetch();
            message.success(res.data.NewOrdenAsigRider.messages);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      message.warning("Debes seleccionar un rider para continuar");
    }
  };

  const onChangePage = (page, pageSize) => {
    setpage(page);
    setlimit(pageSize);
  };

  return (
    <div>
      <div>
        <div className="search_container">
          <div>
            <h3>Filtrar por fecha o ID {count}</h3>
            <Select
              allowClear
              value={tipo}
              style={{
                width: "auto",
                height: 35,
                marginRight: 10,
              }}
              placeholder="Elige un tipo"
              onChange={handleChangeTipo}
            >
              <Option value="Periodo">Periodo</Option>
              <Option value="Sólo un día">Sólo un día</Option>
            </Select>
            {tipo === "Periodo" ? (
              <RangePicker
                locale={locate}
                defaultValue={[
                  moment(fromDate, dateFormat),
                  moment(toDate, dateFormat),
                ]}
                format={dateFormat}
                onChange={onDateRangeChange}
                style={{
                  height: 35,
                  marginRight: 10,
                  marginBottom: 15,
                }}
              />
            ) : (
              <DatePicker
                onChange={onDateRangeChange}
                style={{
                  height: 35,
                  marginRight: 10,
                  marginBottom: 15,
                }}
                defaultValue={moment(fromDate, dateFormat)}
                format={dateFormat}
                locale={locate}
              />
            )}
            <br />
            <Select
              defaultValue={city}
              showSearch
              style={{ width: 363 }}
              placeholder="Selectcciona una ciudad"
              optionFilterProp="children"
              allowClear
              onChange={onChange}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {dataMunicipio.map((data, i) => {
                return (
                  <Option value={data.nombre} key={i}>
                    {data.nombre}
                  </Option>
                );
              })}
            </Select>

            <br />

            <Query query={query.RESTAURANT} variables={{ city: city }}>
              {(response) => {
                if (response) {
                  const restaurants =
                    response && response.data && response.data.getRestaurant
                      ? response.data.getRestaurant.data
                      : [];

                  return (
                    <Select
                      mode="multiple"
                      allowClear
                      style={{
                        width: "auto",
                        minWidth: 363,
                        marginTop: 15,
                      }}
                      placeholder="Selecciona uno o varios establecimientos"
                      onChange={handleChange}
                      onSearch={onSearchStore}
                      loading={response.loading}
                    >
                      {restaurants.map((store, i) => {
                        return (
                          <Option key={i} value={store._id}>
                            {store.title}
                          </Option>
                        );
                      })}
                    </Select>
                  );
                }
              }}
            </Query>
            <br />
            <Query query={query.GET_RIDER_ALL}>
              {(response) => {
                if (response) {
                  const riders =
                    response &&
                    response.data &&
                    response.data.getRiderForAdminAll
                      ? response.data.getRiderForAdminAll.data
                      : [];

                  return (
                    <Select
                      mode="multiple"
                      allowClear
                      style={{
                        width: 363,
                        marginTop: 15,
                      }}
                      placeholder="Selecciona uno o varios repartidores"
                      onChange={onChangeRiders}
                      loading={response.loading}
                    >
                      {riders.map((rider, i) => {
                        return (
                          <Option key={i} value={rider._id}>
                            {rider.name} {rider.lastName}
                          </Option>
                        );
                      })}
                    </Select>
                  );
                }
              }}
            </Query>
            <br />
            <Button
              onClick={() => setIsModalReportVisible(true)}
              style={{
                marginTop: 15,
                height: 40,
                width: 363,
              }}
              type="primary"
            >
              OBTENER REPORTE
            </Button>
            <Statistic
              title="Total de pedidos"
              value={count}
              style={{ marginTop: 25 }}
            />
          </div>
          <div>
            <Search
              style={{ width: "100%", minWidth: 363, marginTop: 15 }}
              placeholder="Busca un pedido por ID"
              allowClear
              type="number"
              enterButton="Buscar"
              size="large"
              onSearch={onSearch}
              maxLength={6}
            />
          </div>
        </div>
        <Card
          loading={loading}
          datos={orders}
          setpage={setpage}
          setlimit={setlimit}
          openRider={openRider}
          refetch={refetch}
          riderID={riderID}
          dataOrder={dataOrder}
          setriderID={setriderID}
          onChangePage={onChangePage}
          count={count}
          limit={limit}
        />
      </div>

      {dataOrder ? (
        <Modal
          title={false}
          visible={isModalVisible}
          okText="Asignar"
          cancelText="Cancelar"
          onOk={() => asigRider()}
          okButtonProps={{ disabled: riderID ? false : true }}
          onCancel={() => {
            setIsModalVisible(false);
            setriderID(null);
          }}
        >
          <div className="modal_contents">
            <Query
              query={query.GET_RIDER}
              variables={{ city: dataOrder.storeData.city }}
            >
              {(response) => {
                if (response.loading) {
                  return (
                    <div className="modal_contents_loading">
                      <Spin
                        indicator={
                          <LoadingOutlined style={{ fontSize: 34 }} spin />
                        }
                      />
                    </div>
                  );
                }
                if (response) {
                  const riders =
                    response && response.data && response.data.getRiderForAdmin
                      ? response.data.getRiderForAdmin.data
                      : [];

                  riders.sort((a, b) =>
                    a.numberCurrentOrder > b.numberCurrentOrder
                      ? 1
                      : b.numberCurrentOrder > a.numberCurrentOrder
                      ? -1
                      : 0
                  );

                  response.refetch();

                  return (
                    <List
                      style={{ paddingTop: 50, paddingBottom: 50 }}
                      itemLayout="horizontal"
                      dataSource={riders}
                      renderItem={(item) => (
                        <List.Item
                          onClick={() => setriderID(item._id)}
                          style={{ cursor: "pointer" }}
                        >
                          <Popover
                            content={
                              <div>{RiderOrder(item._id, item.city)}</div>
                            }
                            title={`Pedidos asignados ${item.name}`}
                          >
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  src={IMAGES_PATH + item.avatar}
                                  size={50}
                                />
                              }
                              title={
                                <div style={{ display: "flex" }}>
                                  <h4
                                    style={{ margin: 0 }}
                                  >{`${item.name} ${item.lastName}`}</h4>
                                  {item.isAvalible ? (
                                    <Tag
                                      color="#87d068"
                                      style={{
                                        marginLeft: 10,
                                        borderRadius: 100,
                                      }}
                                    >
                                      Abierto
                                    </Tag>
                                  ) : (
                                    <Tag
                                      color="#F5365C"
                                      style={{
                                        marginLeft: 10,
                                        borderRadius: 100,
                                      }}
                                    >
                                      Cerrado
                                    </Tag>
                                  )}
                                </div>
                              }
                              description={
                                <div>
                                  {item.numberCurrentOrder === 0 ? (
                                    <Tag color="success">
                                      Disponible sin pedidos asignado
                                    </Tag>
                                  ) : (
                                    <>
                                      {item.numberCurrentOrder > 3 ? (
                                        <Tag color="error">
                                          Muchos pedidos asignado: más de{" "}
                                          {item.numberCurrentOrder} pedidos
                                        </Tag>
                                      ) : (
                                        <Tag color="orange">
                                          Tiene asignada ahora:{" "}
                                          {item.numberCurrentOrder}{" "}
                                          {item.numberCurrentOrder > 1
                                            ? "pedidos"
                                            : "pedido"}
                                        </Tag>
                                      )}
                                    </>
                                  )}
                                </div>
                              }
                            />
                          </Popover>
                          {riderID === item._id ? (
                            <CheckCircleFilled
                              style={{ color: "#90c33c", fontSize: 18 }}
                            />
                          ) : null}
                        </List.Item>
                      )}
                    />
                  );
                }
              }}
            </Query>
          </div>
        </Modal>
      ) : null}

      <Report
        setIsModalVisible={setIsModalReportVisible}
        isModalVisible={isModalReportVisible}
      />
    </div>
  );
}
