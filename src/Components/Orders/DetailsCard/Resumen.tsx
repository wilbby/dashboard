import React from "react";
import "./index.css";
import { formaterPrice } from "../../../Utils/formatePrice";
import { Button } from "antd";

export default function Items(props: any) {
  const { data, title } = props;

  var totals = 0;

  data.items.forEach(function (items: any) {
    const t = items.items.quantity * items.items.price;
    totals += t;
    items.items.subItems.forEach((x: any) => {
      const s = x.quantity * x.price;
      totals += s * items.items.quantity;
      const t = x && x.subItems ? x.subItems : [];
      t.forEach((y: any) => {
        const u = y.quantity * y.price;
        totals += u * items.items.quantity;
      });
    });
  });

  return (
    <div className="items-cart">
      <h3>{title}</h3>

      <div className="items-cart-resumen">
        <div>
          <ul>
            <li>
              <p>Productos:</p>
            </li>
            <li>
              <p>Tarifa de servicio:</p>
            </li>
            <li>
              <p>Entrega:</p>
            </li>
            <li>
              <p>Propina:</p>
            </li>
            <li>
              <p>Descuento:</p>
            </li>
            <li>
              <h1>Total:</h1>
            </li>
          </ul>
        </div>

        <div>
          <ul>
            <li>
              <p>{formaterPrice(totals / 100, "es-ES", "EUR")}</p>
            </li>
            <li>
              <p>{formaterPrice(data.serviceCharge / 100, "es-ES", "EUR")}</p>
            </li>
            <li>
              <p>{formaterPrice(data.deliveryCost / 100, "es-ES", "EUR")}</p>
            </li>
            <li>
              <p>{formaterPrice(data.tip / 100, "es-ES", "EUR")}</p>
            </li>
            <li>
              <p>
                {" "}
                - {formaterPrice(data.discountTotal / 100, "es-ES", "EUR")}
              </p>
            </li>
            <li>
              <h1>{formaterPrice(data.payment / 100, "es-ES", "EUR")}</h1>
            </li>
          </ul>
        </div>
      </div>
      <Button
        type="primary"
        style={{ width: "100%", marginTop: 20 }}
        href={data.invoiceUrl}
        target="_blank"
      >
        Ver factura del cliente
      </Button>
    </div>
  );
}
