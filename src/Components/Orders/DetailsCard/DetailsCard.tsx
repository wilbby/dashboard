import React from "react";
import "./index.css";
import Items from "./Items";
import Products from "./Products";
import Resumen from "./Resumen";
import PaymentType from "./PaymentType";
import Hora from "./Hora";
import Nota from "./Nota";
import Status from "./Status";
import Distance from "./Distance";
import { message } from "antd";
import moment from "moment";
import Cupon from "./Cupon";
import ReactJson from "react-json-view";

export default function DetailsCard(props: any) {
  const { data, refetch, NewOrdenProceed } = props;

  const deliveretOrder = () => {
    refetch();
    NewOrdenProceed({
      variables: {
        ordenId: data._id,
        status: "Entregada",
        IntegerValue: 120,
        statusProcess: {
          status: "Entregada",
          date: moment(new Date()).utcOffset(60).format(),
        },
      },
    }).then((res: any) => {
      refetch();
      message.success("El pedido ha sido marcado como entregado");
    });
  };

  return (
    <div className="new-menu-cart-container">
      <div className="new-cart-wrapper">
        <div className="new-cart-content">
          <div className="carts">
            <Items
              title="Info del cliente"
              title2={`${data.customerData.name} ${data.customerData.lastName}`}
              sub={data.customerData.telefono}
              sub1={data.customerData.email}
            />
            <Items
              title="Info del establecimiento"
              title2={data.storeData.title}
              p={`${data.storeData.adress.calle},
                ${data.storeData.adress.numero},
                ${data.storeData.adress.codigoPostal}, 
                ${data.storeData.city}`}
              sub={data.storeData.phone}
              sub1={data.storeData.email}
            />
            {data.courierData && data.courier ? (
              <Items
                title="Info del repartidor"
                title2={`${data.courierData.name} ${data.courierData.lastName}`}
                sub={data.courierData.telefono}
                sub1={data.courierData.email}
              />
            ) : null}
            {data.orderType === "delivery" && data.deliveryAddressData ? (
              <Items
                title="Dirección de entrega"
                p={`${data.deliveryAddressData.formatted_address},
                ${data.deliveryAddressData.puertaPiso},
                ${
                  data.deliveryAddressData.postalcode
                    ? data.deliveryAddressData.postalcode
                    : "No postalcode"
                },
                ${data.deliveryAddressData.city}, ${
                  data.deliveryAddressData.type
                }`}
              />
            ) : null}
            <Status title="Estado del pedido" data={data} />
          </div>
          <div
            className="carts"
            style={{ width: "55%", marginRight: 40, minWidth: 500 }}
          >
            <Products title="Productos" data={data} />
          </div>
          <div className="carts">
            <Hora title="Hora de entrega" data={data} />
            <Nota title="Nota del pedido" nota={data.note} data={data} />
            <Distance title="Distancia" data={data} />
            {data.cupon ? <Cupon title="Cupon" data={data} /> : null}
            <Resumen title="Resumen" data={data} />
            <PaymentType title="Método de pago" data={data} />
          </div>
        </div>
      </div>
      <div className="JSON_VIEW">
        <ReactJson src={data} name="Data del pedido" collapsed={true} />
      </div>
    </div>
  );
}
