import React from "react";
import "./index.css";

export default function Items(props: any) {
  const { data, title } = props;

  const report = data.reportRiders ? data.reportRiders : {};

  const displayDistance = report.km ? Number(report.km) : 310;
  const dist = displayDistance / 100;

  return (
    <div className="items-cart">
      <div className="items-cart-horario">
        <h3>{title}</h3>
        <h1 style={{ fontWeight: "bold", color: "#90c33c" }}>
          {dist.toFixed(1)} km
        </h1>
      </div>
      <div className="items-cart-horario">
        <h3>Tipo de restaurante</h3>
        <h1
          style={{
            fontWeight: "bold",
            color: data.storeData.ispartners ? "#90c33c" : "#ff4d4f",
          }}
        >
          {data.storeData.ispartners ? "Partner asociado" : "No es partner"}
        </h1>
      </div>
    </div>
  );
}
