import React from "react";
import {
  ExclamationCircleOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
  RollbackOutlined,
  CheckOutlined,
  SyncOutlined,
  ClockCircleOutlined,
} from "@ant-design/icons";
import "./card.css";
import { Modal, Table, Tag, Button, message } from "antd";
import Details from "../DetailsCard";
import { useMutation } from "react-apollo";
import { mutation } from "../../../GraphQL";
import moment from "moment";
import "moment/locale/es";
import { Countdown } from "../time";

const { confirm } = Modal;

export default function Card(props: any) {
  const {
    loading,
    datos,
    openRider,
    setpage,
    setlimit,
    onChangePage,
    refetch,
    count,
    limit,
  } = props;

  const [NewOrdenProceed] = useMutation(mutation.NEW_ORDER_PROCEED);

  const returOrder = (orders: any) => {
    refetch();
    NewOrdenProceed({
      variables: {
        ordenId: orders._id,
        status: "Devuelto",
        IntegerValue: 0,
        statusProcess: {
          status: "Devuelto",
          date: moment(new Date()).utcOffset(60).format(),
        },
      },
    }).then((res: any) => {
      refetch();
      message.success("El pedido ha sido marcado como devuelto");
    });
  };

  const proccessOrder = (orders: any, status: string, value: number) => {
    refetch();
    NewOrdenProceed({
      variables: {
        ordenId: orders._id,
        status: status,
        IntegerValue: value,
        statusProcess: {
          status: status,
          date: moment(new Date()).utcOffset(60).format(),
        },
      },
    })
      .then((res: any) => {
        refetch();
        message.success("Pedido actualizado");
      })
      .catch(() => {
        message.error("Algo salio mal intenlo de nuevo");
      });
  };

  const renderTag = (d: any) => {
    if(d.paymentMethod === "Datafono") {
      return <>
      <Tag color="#fa541c">Pago con datáfono</Tag> <br />
      </>
    } else if(d.paymentMethod === "Efectivo") {
      return <>
      <Tag color="#87d045">Pago en Efectivo</Tag> <br />
      </>
    } 
  }

  const renderBTNStatus = (order: any) => {
    switch (order.status) {
      case "Nueva":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "Confirmada", 40)}
          >
            Confirmar pedido
          </Button>
        );

      case "Confirmada":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "En la cocina", 50)}
          >
            Pasar a la cocina
          </Button>
        );

      case "En la cocina":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "Listo para recoger", 60)}
          >
            Listo para recoger
          </Button>
        );

      case "Listo para recoger":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "Preparando para el envío", 70)}
          >
            Preparando para el envío
          </Button>
        );

      case "Preparando para el envío":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "En camino", 80)}
          >
            En camino
          </Button>
        );

      case "En camino":
        return (
          <Button
            type="primary"
            onClick={() => proccessOrder(order, "Entregada", 90)}
          >
            Entregada
          </Button>
        );

      case "Devuelto":
        return (
          <Button
            danger
            type="primary"
            onClick={() => null}
            disabled={order.status === "Devuelto"}
          >
            Devuelto
          </Button>
        );

      default:
        return (
          <Button type="primary" onClick={() => null}>
            Pedido completado
          </Button>
        );
    }
  };

  function showDeleteConfirm(data: any) {
    confirm({
      title: "¿Estás seguro que deseas hacer la devolución?",
      icon: <ExclamationCircleOutlined />,
      content: "Lamentamos no poder terminar este pedido",
      okText: "Devolver",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        returOrder(data);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const columns = [
    {
      title: "Orden ID",
      dataIndex: "",
      key: "",
      render: (d: any) => {
        return (
          <div>
            <Tag color="#f50">#{d.channelOrderDisplayId}</Tag>
            <br />
            <p style={{ marginTop: 5 }}>
              <ClockCircleOutlined />{" "}
              {moment(d.created_at).utcOffset(60).format("HH:mm")}
            </p>
          </div>
        );
      },
    },
    {
      title: "Cliente",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        return (
          <div>
            <p style={{ margin: 0, padding: 0 }}>
              {d.customerData.name} {d.customerData.lastName}
            </p>

            {renderTag(d)}

            {d.orderType === "pickup" ? null : (
              <>
              {d.deliveryAddressData ? <span style={{ color: "gray" }}>
                {d.deliveryAddressData.formatted_address},{" "}
                {d.deliveryAddressData.puertaPiso},{" "}
                {` ${d.deliveryAddressData.postalcode}`
                  ? d.deliveryAddressData.postalcode
                  : " No postalcode"}
              </span> : "Dirección de entrega no disponible"}
              </>
              
            )}
          </div>
        );
      },
    },
    {
      title: "Partner",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        return (
          <div>
            <p style={{ margin: 0, padding: 0 }}>{d.storeData.title}</p>
            <span style={{ color: "#a0a0a0" }}>{d.storeData.city}</span>
            <br />
            {d.storeData.ispartners ? null : (
              <Tag color="red">Partner no asociado</Tag>
            )}
          </div>
        );
      },
    },
    {
      title: "Rider",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        return (
          <div>
            {!d.storeData.autoshipping ? (
              <div>
                {d.orderType === "delivery" ? (
                  <div>
                    {d.courier ? (
                      <Button
                        type="dashed"
                        onClick={() => openRider(d)}
                        disabled={d.status === "Devuelto" ? true : false}
                      >
                        {d.courierData.name} {d.courierData.lastName}
                      </Button>
                    ) : (
                      <Button
                        type="dashed"
                        onClick={() => openRider(d)}
                        disabled={d.status === "Devuelto" ? true : false}
                      >
                        Asignar
                      </Button>
                    )}
                  </div>
                ) : (
                  <Button type="dashed" danger>
                    Para recoger
                  </Button>
                )}
              </div>
            ) : (
              <Button type="dashed" danger>
                Entregado por la tienda
              </Button>
            )}
          </div>
        );
      },
    },
    {
      title: "Estado",
      dataIndex: "",
      key: "x",
      render: (d: any) => {
        const renderStatus = () => {
          switch (d.status) {
            case "Rechazada por el rider":
              return (
                <Tag color="warning" icon={<ExclamationCircleOutlined />}>
                  {d.status}
                </Tag>
              );
            case "Rechazada por la tienda":
              return (
                <Tag icon={<CloseCircleOutlined />} color="error">
                  {d.status}
                </Tag>
              );
            case "Resolución":
              return (
                <Tag icon={<CloseCircleOutlined />} color="error">
                  {d.status}
                </Tag>
              );
            case "Devuelto":
              return (
                <Tag icon={<RollbackOutlined />} color="error">
                  {d.status}
                </Tag>
              );
            case "Nueva":
              return (
                <Tag icon={<CheckOutlined />} color="warning">
                  {d.status}
                </Tag>
              );

            case "Entregada":
              return (
                <Tag icon={<CheckCircleOutlined />} color="success">
                  {d.status}
                </Tag>
              );

            case "Finalizada":
              return (
                <Tag icon={<CheckCircleOutlined />} color="success">
                  {d.status}
                </Tag>
              );
            default:
              return (
                <Tag icon={<SyncOutlined spin />} color="processing">
                  {d.status}
                </Tag>
              );
          }
        };
        return (
          <div>
            {renderStatus()}{" "}
            <Countdown
              eventTime={Number(
                moment(d.scheduled ? d.deliveryTime : d.created_at)
                  .utcOffset(60)
                  .add(45, "minutes")
                  .format("X")
              )}
              interval={1000}
              scheduled={d.scheduled}
              status={d.status}
            />
          </div>
        );
      },
    },

    {
      title: "Acción",
      dataIndex: "",
      key: "x",
      render: (data: any) => {
        return renderBTNStatus(data);
      },
    },

    {
      title: "Devolver",
      dataIndex: "",
      key: "x",
      render: (data: any) => {
        return (
          <Button
            danger
            type="primary"
            onClick={() => showDeleteConfirm(data)}
            disabled={data.status === "Devuelto"}
          >
            {data.status === "Devuelto" ? "Devuelto" : "Hacer devolución"}
          </Button>
        );
      },
    },
  ];

  return (
    <Table
      scroll={{ x: "calc(100vh - 4em)" }}
      rowKey={(record) => record.channelOrderDisplayId}
      onChange={(pagination) => {
        setpage(pagination.current);
        setlimit(pagination.pageSize);
      }}
      columns={columns}
      loading={loading}
      expandable={{
        expandedRowRender: (record) => {
          return (
            <Details
              data={record}
              refetch={refetch}
              NewOrdenProceed={NewOrdenProceed}
            />
          );
        },
        rowExpandable: (record) =>
          record.channelOrderDisplayId === record.channelOrderDisplayId,
      }}
      dataSource={datos}
      pagination={{
        defaultCurrent: 1,
        onChange: onChangePage,
        total: count,
        pageSize: limit,
      }}
    />
  );
}
