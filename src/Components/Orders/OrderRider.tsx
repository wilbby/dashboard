import { useQuery } from "react-apollo";
import { query } from "../../GraphQL";
import { Tag } from "antd";
import "./index.css";
import { Spin, Result } from "antd";
import { SmileOutlined } from "@ant-design/icons";

export const RiderOrder = (rider: string, city: string) => {
  const { data, loading, refetch } = useQuery(query.GET_ORDER, {
    variables: {
      page: 1,
      limit: 10,
      status: [
        "Nueva",
        "Confirmada",
        "En la cocina",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
      ],
      city: city,
      rider: rider,
    },
  });

  refetch();

  const order =
    data && data.getNewOrderAdimin ? data.getNewOrderAdimin.list : [];

  return (
    <div>
      {loading ? (
        <div
          style={{
            width: "100%",
            minWidth: 400,
            height: "25vh",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          <Spin />

          <p style={{ marginLeft: 10, marginTop: 5 }}>Loading ....</p>
        </div>
      ) : (
        <>
          {order && order.length > 0 ? (
            <>
              {order &&
                order.map((item: any, i: number) => {
                  return (
                    <div key={i} className="items">
                      <div
                        style={{
                          paddingRight: 20,
                          display: "flex",
                          flexDirection: "row",
                          justifyContent: "flex-start",
                          alignItems: "center",
                        }}
                      >
                        <Tag color="orange">#{item.channelOrderDisplayId}</Tag>
                        <div>
                          <h2>{item.storeData.title}</h2>
                          {item.orderType === "pickup" ? null : (
                            <p style={{ color: "gray", margin: 0 }}>
                              {item.deliveryAddressData ? item.deliveryAddressData.formatted_address : "Dirección de entrega no disponible"}
                            </p>
                          )}
                        </div>
                      </div>
                      <Tag color="green" style={{ marginLeft: 20 }}>
                        {item.status}
                      </Tag>
                    </div>
                  );
                })}
            </>
          ) : (
            <Result
              icon={<SmileOutlined style={{ fontSize: 20 }} />}
              title={<p>Aún no tiene pedidos asignado</p>}
            />
          )}
        </>
      )}
    </div>
  );
};
