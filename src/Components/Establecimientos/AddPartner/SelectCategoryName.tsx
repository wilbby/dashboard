import React from "react";
import { Select } from "antd";

const { Option } = Select;

export default function SelectCategory(props: any) {
  const { handleChange } = props;

  const category = [
    {
      id: 1,
      title: "Restaurantes",
    },

    {
      id: 1,
      title: "Tiendas",
    },

    {
      id: 1,
      title: "Supermercados",
    },
  ];

  return (
    <Select
      allowClear
      style={{
        width: "45%",
        marginRight: 15,
        marginTop: 15,
      }}
      placeholder="Selecciona una categoria"
      onChange={handleChange}
    >
      {category.map((item: any, i: number) => {
        return (
          <Option key={i} value={item.title}>
            {item.title}
          </Option>
        );
      })}
    </Select>
  );
}
