import React from "react";
import { Select } from "antd";
import { useQuery } from "react-apollo";
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function SelectCategory(props: any) {
  const { handleChange } = props;
  const { data, loading } = useQuery(query.CATEGORY);

  const category = data && data.getCategory ? data.getCategory.data : [];

  return (
    <Select
      mode="multiple"
      allowClear
      style={{
        width: "45%",
        marginRight: 15,
      }}
      placeholder="Selecciona una o varias categorias"
      onChange={handleChange}
      loading={loading}
    >
      {category.map((item: any, i: number) => {
        return (
          <Option key={i} value={item._id}>
            {item.title}
          </Option>
        );
      })}
    </Select>
  );
}
