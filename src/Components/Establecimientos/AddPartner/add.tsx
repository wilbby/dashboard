import React, { useState } from "react";
import "./add.css";
import {
  Modal,
  Tooltip,
  Upload,
  Input,
  InputNumber,
  Switch,
  message,
  Button,
} from "antd";
import {
  PlusCircleOutlined,
  LoadingOutlined,
  InstagramOutlined,
  FacebookOutlined,
  TwitterOutlined,
  GlobalOutlined,
} from "@ant-design/icons";
import { Query, useMutation } from "react-apollo";
import { mutation, query } from "../../../GraphQL";
import SelectCategory from "./SelectCategory";
import SelectTipo from "./SelectTipo";
import SelectTipoTienda from "./SelectTipoTienda";
import SelectAltaCocina from "./SelectAltacocina";
import SelectCatName from "./SelectCategoryName";

const { TextArea } = Input;

const sheduledData = {
  Lunes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Martes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Miércoles: [
    {
      star: 9,
      end: 20,
    },
  ],

  Jueves: [
    {
      star: 9,
      end: 20,
    },
  ],

  Viernes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Sábado: [
    {
      star: 9,
      end: 14,
    },
  ],

  Domingo: [],
};

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function AddPartner(props: any) {
  const [imagen, setimagen] = useState("");
  const [logo, setiLogo] = useState("");
  const [categoryID, setCategoryID] = useState([]);
  const [categoryName, setCategoryName] = useState([]);
  const [tipo, setTipo] = useState([]);
  const [loadingImage, setloadingImage] = useState(false);
  const [loadinglogo, setloadinglogo] = useState(false);
  const [city, setCity] = useState("");
  const [lat, setlat] = useState("");
  const [lgn, setlgn] = useState("");
  const [calle, setCalle] = useState("");
  const [cp, setCp] = useState("");

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [shipping, setShipping] = useState(0);
  const [shippingLow, setShippingLow] = useState(0);
  const [service, setService] = useState(0);
  const [minime, setMinime] = useState(0);
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [stimated, setStimated] = useState("");
  const [slug, setSlug] = useState("");
  const [tagOffert, setTagOffert] = useState("");
  const [tipe, setTipe] = useState("");
  const [isParnet, setisParnet] = useState(true);
  const [isnew, setIsnew] = useState(true);
  const [open, setOpen] = useState(true);
  const [takeAway, setTakeawai] = useState(true);
  const [autoshipping, setAutoshipping] = useState(false);
  const [offert, setOffert] = useState(false);
  const [noScheduled, setNoscheduled] = useState(false);
  const [cashPayment, setCashPayment] = useState(false);
  const [otter, setOtter] = useState(false);
  const [highkitchen, setHighkitchen] = useState(false);
  const [deliverect, setdeliverect] = useState(false);
  const [ordatic, setOrdatic] = useState(false);
  const [scheduleOnly, setScheduleOnly] = useState(false);
  const [collections, setcollections] = useState(false);
  const [scheduleOnlyHour, setScheduleOnlyHour] = useState(0);
  const [instagram, setinstagram] = useState("");
  const [facebook, setfacebook] = useState("");
  const [twitter, settwitter] = useState("");
  const [web, setweb] = useState("");
  const [numero, setNumero] = useState("");
  const [Alergenos, setAlergenos] = useState("");
  const [forttalezaURL, setforttalezaURL] = useState("");

  const [openURL, setopenURL] = useState(false);
  const [url, seturl] = useState("");
  const [aceptReservation, setaceptReservation] = useState(false);
  const [showAlergeno, setshowAlergeno] = useState(false);
  const [inTop, setinTop] = useState(false);
  const [sort, setsort] = useState(1);
  const [inHouse, setinHouse] = useState(false);
  const [adsInHouse, setadsInHouse] = useState(false);

  

  
        // horario

  const [singleUploadToStoreImagenAws] = useMutation(
    mutation.UPLOAD_FILE_STORE
  );

  const [createRestaurant] = useMutation(mutation.CREATED_STORE);

  const { visible, setVisibleAdd, refetch } = props;

  const input = {
    title: title,
    image: imagen,
    description: description,
    shipping: shipping * 100,
    extras: service * 100,
    minime: minime * 100,
    rating: "0.0",
    diaslaborales: [
      "Lunes",
      "Marte",
      "Miécoles",
      "Jueves",
      "Viernes",
      "Sábado",
      "Domingo",
    ],
    categoryName: categoryName,
    categoryID: categoryID,
    phone: `+34 ${phone}`,
    email: email,
    logo: logo,
    password: "admin",
    type: tipe,
    tipo: tipo,
    schedule: {
      Monday: {
        day: "Lunes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Tuesday: {
        day: "Martes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Wednesday: {
        day: "Miércoles",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Thursday: {
        day: "Jueves",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Friday: {
        day: "Viernes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Saturday: {
        day: "Sábado",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Sunday: {
        day: "Domingo",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
    },
    ispartners: isParnet,
    isnew: isnew,
    llevar: takeAway,
    alegeno_url: Alergenos,
    autoshipping: autoshipping,
    open: open,
    previous_shipping: shippingLow === 0 ? null : shippingLow * 100,
    stimateTime: stimated,
    slug: slug,
    highkitchen: highkitchen,
    inOffert: offert,
    includeCity: [city],
    OnesignalID: "",
    channelLinkId: "",
    collections: collections,
    isDeliverectPartner: deliverect,
    salvingPack: {},
    isOrdaticPartner: ordatic,
    isOrdaticPartnerID: "",
    adress: {
      calle: calle,
      numero: numero,
      codigoPostal: cp,
      ciudad: city,
      lat: lat,
      lgn: lgn,
    },
    city: city,
    contactCode: "",
    socialLink: {
      instagram: instagram,
      facebook: facebook,
      twitter: twitter,
      web: web,
    },
    isOtterPartner: otter,
    OtterPartnerId: "",
    scheduleOnly: {
      available: scheduleOnly,
      hour: scheduleOnlyHour * 24,
    },
    shorting: 0,
    noScheduled: noScheduled,
    cashPayment: cashPayment,
    tagOffert: tagOffert ? tagOffert : null,
    forttalezaURL: forttalezaURL,
    openURL: openURL,
    url: url,      
    aceptReservation: aceptReservation,
    reservationScheduled: sheduledData,
    showAlergeno: showAlergeno
  };

  const handleCancel = () => {
    setVisibleAdd(false);
  };

  function handleChange(value: any) {
    setCategoryID(value);
  }

  function handleChangeCatName(value: any) {
    setCategoryName(value);
  }

  function handleChangeTipe(value: any) {
    setTipo(value);
  }

  const createdStore = () => {
    createRestaurant({ variables: { input: { data: input } } })
      .then((res: any) => {
        const result = res.data.createRestaurant;
        if (result.success) {
          message.success(result.messages);
          refetch();
          setVisibleAdd(false);
        } else {
          message.warning(result.messages);
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };
  const uploadButton = (
    <Tooltip title="Añadir foto de portada">
      <div
        style={{
          width: 450,
          height: 200,
          borderRadius: 10,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadingImage ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        )}

        <div className="ant-upload-text">
          <span style={{ fontSize: 12 }}>
            Añadir foto de portada 450px X 200px
          </span>
        </div>
      </div>
    </Tooltip>
  );

  const uploadButtonAvatar = (
    <Tooltip title="Añadir logo del establecimiento">
      <div
        style={{
          width: 100,
          height: 100,
          borderRadius: 10,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadinglogo ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        )}

        <div className="ant-upload-text">
          <span style={{ fontSize: 12 }}>Añadir logo 150px X 150px</span>
        </div>
      </div>
    </Tooltip>
  );

  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      onOk={createdStore}
      okButtonProps={{ disabled: lat && lgn ? false : true }}
      okText="Añadir establecimiento"
      cancelText="Cancelar"
      width={700}
    >
      <div style={{ minHeight: 700, padding: 20 }}>
        {
          //@ts-ignore
          <h4>{`Añadir establecimiento`}</h4>
        }

        <div>
          <div
            style={{
              width: 450,
              height: 200,
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: 30,
            }}
          >
            <Upload
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              customRequest={async (data) => {
                setloadingImage(true);
                let file = await getBase64(data.file);
                singleUploadToStoreImagenAws({
                  variables: { file },
                })
                  .then((res: any) => {
                    setloadingImage(false);
                    setimagen(
                      res.data.singleUploadToStoreImagenAws.data.Location
                    );
                  })
                  .catch((error: any) => {
                    setloadingImage(false);
                    message.error(
                      "Imagen muy grande reduce el tamaño de la misma"
                    );
                  });
              }}
            >
              {imagen ? (
                <Tooltip title="Haz click para cambiar">
                  <img className="imagen_prod_add" src={imagen} />
                </Tooltip>
              ) : null}

              {!imagen ? uploadButton : null}
            </Upload>
          </div>

          <div
            style={{
              width: 100,
              height: 100,
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: -40,
            }}
          >
            <Upload
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              customRequest={async (data) => {
                setloadinglogo(true);
                let file = await getBase64(data.file);

                singleUploadToStoreImagenAws({ variables: { file } })
                  .then((res: any) => {
                    setloadinglogo(false);
                    setiLogo(
                      res.data.singleUploadToStoreImagenAws.data.Location
                    );
                  })
                  .catch((error: any) => {
                    setloadinglogo(false);
                    message.error(
                      "Imagen muy grande reduce el tamaño de la misma"
                    );
                  });
              }}
            >
              {logo ? (
                <Tooltip title="Haz click para cambiar">
                  <img className="imagen_prod_add_logo" src={logo} />
                </Tooltip>
              ) : null}

              {!logo ? uploadButtonAvatar : null}
            </Upload>
          </div>
        </div>

        <div
          style={{
            textAlign: "center",
            marginTop: 30,
          }}
        >
          <Button
            type="primary"
            href="https://www.iloveimg.com/es/comprimir-imagen"
            target="_blank"
          >
            Comprimir imagen
          </Button>
        </div>

        <div className="Forma" style={{ marginTop: 30 }}>
          <Input
            placeholder="Nombre el establecomiento"
            onChange={(e) => setTitle(e.target.value)}
          />
          <TextArea
            placeholder="Descripción"
            rows={4}
            style={{ marginTop: 15, height: 120 }}
            showCount
            maxLength={150}
            onChange={(e) => setDescription(e.target.value)}
          />
          <div style={{ marginTop: 30 }}>
            <InputNumber
              type="number"
              placeholder="Precio de envío"
              min={0}
              style={{ width: 130, marginRight: 15, marginTop: 10 }}
              onChange={(e) => setShipping(Number(e))}
            />
            <InputNumber
              type="number"
              placeholder="Envío rebajado"
              min={0}
              style={{ width: 130, marginRight: 15, marginTop: 10 }}
              onChange={(e) => setShippingLow(Number(e))}
            />
            <InputNumber
              type="number"
              placeholder="Tarifa servicio"
              min={0}
              style={{ width: 130, marginRight: 15, marginTop: 10 }}
              onChange={(e) => setService(Number(e))}
            />
            <InputNumber
              type="number"
              placeholder="Minímo"
              min={0}
              style={{ width: 130, marginTop: 10 }}
              onChange={(e) => setMinime(Number(e))}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Email"
              type="email"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Input
              placeholder="Teléfono"
              type="tel"
              style={{ width: "50%" }}
              onChange={(e) => setPhone(e.target.value)}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Tiempo estimado (35-45 min)"
              type="name"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setStimated(e.target.value)}
            />
            <Input
              placeholder="Slug Elemplo (el-manantial)"
              type="name"
              style={{ width: "50%" }}
              onChange={(e) => setSlug(e.target.value)}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Ofertas o Promociones (50% Off)"
              type="name"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setTagOffert(e.target.value)}
            />
            <Input
              placeholder="Tipo Ejm: (Asiática | Sushi)"
              type="name"
              style={{ width: "50%" }}
              onChange={(e) => setTipe(e.target.value)}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <SelectCategory handleChange={handleChange} />
            <br />
            <SelectCatName handleChange={handleChangeCatName} />
            {categoryID.filter((x: any) => x === "5fb7a32cb234a46c09297804")
              .length > 0 ? (
              <>
                {highkitchen ? (
                  <SelectAltaCocina handleChange={handleChangeTipe} />
                ) : (
                  <SelectTipo handleChange={handleChangeTipe} />
                )}
              </>
            ) : null}
            {categoryID.filter((x: any) => x === "5fb7aec4b234a46c0929780b")
              .length > 0 ? (
              <SelectTipoTienda handleChange={handleChangeTipe} />
            ) : null}
          </div>

          <div className="boolean_content">
            <div>
              <Switch
                checkedChildren="Partner Asocioado"
                unCheckedChildren="Partner Asocioado"
                defaultChecked={isParnet}
                onChange={(checked) => setisParnet(checked)}
              />
              <br />
              <Switch
                checkedChildren="Takeaway"
                unCheckedChildren="Takeaway"
                defaultChecked={takeAway}
                style={{ marginTop: 20 }}
                onChange={(checked) => setTakeawai(checked)}
              />
              <br />
              <Switch
                checkedChildren="Destacado"
                unCheckedChildren="Destacado"
                defaultChecked={offert}
                style={{ marginTop: 20 }}
                onChange={(checked) => setOffert(checked)}
              />
              <br />
              <Switch
                checkedChildren="Otter Partner"
                unCheckedChildren="Otter Partner"
                defaultChecked={otter}
                style={{ marginTop: 20 }}
                onChange={(checked) => setOtter(checked)}
              />
            </div>
            <div>
              <Switch
                checkedChildren="Nuevo"
                unCheckedChildren="Nuevo"
                defaultChecked={isnew}
                onChange={(checked) => setIsnew(checked)}
              />
              <br />
              <Switch 
                checkedChildren="Entrega por el partner"
                unCheckedChildren="Entrega por el partner"
                defaultChecked={autoshipping}
                style={{ marginTop: 20 }}
                onChange={(checked) => setAutoshipping(checked)}
              />
              <br />
              <Switch
                checkedChildren="No aceptar programado"
                unCheckedChildren="No aceptar programado"
                disabled={scheduleOnly}
                defaultChecked={noScheduled}
                style={{ marginTop: 20 }}
                onChange={(checked) => setNoscheduled(checked)}
              />
              <br />
              <Switch
                checkedChildren="Deliverect Partner"
                unCheckedChildren="Deliverect Partner"
                defaultChecked={deliverect}
                style={{ marginTop: 20 }}
                onChange={(checked) => setdeliverect(checked)}
              />
            </div>
            <div>
              <Switch
                checkedChildren="Abierto"
                unCheckedChildren="Cerrado"
                defaultChecked={open}
                onChange={(checked) => setOpen(checked)}
              />
              <br />
              <Switch
                checkedChildren="Alta Cocina"
                unCheckedChildren="Alta Cocina"
                defaultChecked={highkitchen}
                style={{ marginTop: 20 }}
                onChange={(checked) => setHighkitchen(checked)}
              />
              <br />
              <Switch
                checkedChildren="Pago en efectivo"
                unCheckedChildren="Pago en efectivo"
                defaultChecked={cashPayment}
                onChange={(checked) => setCashPayment(checked)}
                style={{ marginTop: 20 }}
              />
              <br />
              <Switch
                checkedChildren="Ordatic Partner"
                unCheckedChildren="Ordatic Partner"
                defaultChecked={ordatic}
                onChange={(checked) => setOrdatic(checked)}
                style={{ marginTop: 20 }}
              />
            </div>
          </div>
          <div style={{ marginTop: 30, flexDirection: "row" }}>
            <div>
              <Switch
                checkedChildren="Sólo programado"
                unCheckedChildren="Sólo programado"
                defaultChecked={scheduleOnly}
                disabled={noScheduled}
                onChange={(checked) => {
                  setScheduleOnly(checked);
                  setNoscheduled(false);
                }}
              />
              <InputNumber
                type="number"
                disabled={scheduleOnly ? false : true}
                placeholder="Horas de antelación"
                style={{ width: 170, marginLeft: 15, marginTop: 10 }}
                onChange={(e) => setScheduleOnlyHour(Number(e))}
                min={0}
              />
            </div>

            <div style={{ marginTop: 30, display: "flex" }}>
            <Switch
                checkedChildren="Abrir web"
                unCheckedChildren="Abrir detalle"
                defaultChecked={openURL}
                onChange={(checked) => setopenURL(checked)}
              />
              <br />
              <Switch
                checkedChildren="Aceptar reservas"
                unCheckedChildren="No aceptar reservas"
                defaultChecked={aceptReservation}
                style={{marginLeft: 10}}
                onChange={(checked) => setaceptReservation(checked)}
              />
            </div>

            <h3 style={{ marginTop: 30 }}>Cartas digitales</h3> <br />
            <div style={{ display: "flex" }}>
              <Switch
                checkedChildren="Carta digital"
                unCheckedChildren="Carta digital disponible"
                defaultChecked={inHouse}
                onChange={(checked) => setinHouse(checked)}
              />
              <br />
              <Switch
                checkedChildren="Anuncios en la carta"
                unCheckedChildren="Anuncios en la carta disponible"
                defaultChecked={adsInHouse}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setadsInHouse(checked)}
              />
              <br />
              <Switch
                checkedChildren="Mostrar alérgenos"
                unCheckedChildren="Mostrar alérgenos"
                defaultChecked={showAlergeno}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setshowAlergeno(checked)}
              />
            </div>
            <h3 style={{ marginTop: 30 }}>Lista de Tops</h3> <br />
            <div>
              <Switch
                checkedChildren="Mostrar en Tops"
                unCheckedChildren="Montrando en Tops"
                defaultChecked={inTop}
                onChange={(checked) => setinTop(checked)}
              />

              {inTop ? (
                <InputNumber
                  type="number"
                  placeholder="Posición"
                  value={sort}
                  style={{ width: 170, marginTop: 10, marginLeft: 10 }}
                  onChange={(e) => setsort(Number(e))}
                  min={0}
                />
              ) : null}
            </div>

            <div style={{ marginTop: 30 }}>
              <h3>Mostrar los productos como:</h3>
              <Switch
                checkedChildren="Colecciones"
                unCheckedChildren="Lista"
                defaultChecked={collections}
                onChange={(checked) => setcollections(checked)}
              />
            </div>
          </div>
          <div style={{ marginTop: 30, width: "100%" }}>
            <h3>Dirección del establecomiento</h3>
            <Input
              placeholder="Nombre de la calle"
              onChange={(e) => setCalle(e.target.value)}
              style={{ width: "100%", marginTop: 30 }}
              value={calle}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 30,
              }}
            >
              <div style={{ flexDirection: "row" }}>
                <Input
                  placeholder="Número"
                  onChange={(e) => setNumero(e.target.value)}
                  style={{ width: 180, marginRight: 15 }}
                  value={numero}
                />
                <Input
                  placeholder="Código postal"
                  value={cp}
                  onChange={(e) => setCp(e.target.value)}
                  style={{ width: 180, marginRight: 15 }}
                />
                <Input
                  placeholder="Ciudad"
                  onChange={(e) => setCity(e.target.value)}
                  value={city}
                  style={{ width: 265, marginTop: 20 }}
                />
              </div>
            </div>

            <div
              style={{
                marginTop: 20,
                width: 700,
              }}
            >
              <div style={{ flexDirection: "row" }}>
                <Input
                  placeholder="Latitud (42.3439925)"
                  onChange={(e) => setlat(e.target.value)}
                  style={{ width: "45%", marginRight: 15 }}
                  value={lat}
                />
                <Input
                  placeholder="Longitud (-3.696906)"
                  value={lgn}
                  onChange={(e) => setlgn(e.target.value)}
                  style={{ width: "40%", marginRight: 15 }}
                />
              </div>
            </div>
          </div>
          <div style={{ marginTop: 30, flexDirection: "row" }}>
            <Input
              prefix={<InstagramOutlined />}
              placeholder="URL instagram"
              style={{ marginBottom: 15, width: "48%", marginRight: 15 }}
              onChange={(e) => setinstagram(e.target.value)}
            />
            <Input
              prefix={<FacebookOutlined />}
              placeholder="URL facebook"
              style={{ marginBottom: 15, width: "48%" }}
              onChange={(e) => setfacebook(e.target.value)}
            />
            <Input
              prefix={<TwitterOutlined />}
              placeholder="URL twitter"
              style={{ marginBottom: 15, width: "48%", marginRight: 15 }}
              onChange={(e) => settwitter(e.target.value)}
            />
            <Input
              prefix={<GlobalOutlined />}
              placeholder="Sitio web"
              onChange={(e) => setweb(e.target.value)}
              style={{ marginBottom: 15, width: "48%" }}
            />

            <Input
              placeholder="URL de Alergeno del establecimiento"
              onChange={(e) => setAlergenos(`${e.target.value}`)}
              style={{ marginBottom: 15, width: "48%", marginRight: 15 }}
            />

            <Input
              placeholder="URL de perfil Forttalezza"
              onChange={(e) => setforttalezaURL(`${e.target.value}`)}
              style={{ width: "48%" }}
            />

            <Input
              placeholder="URL de detalle"
              onChange={(e) => seturl(`${e.target.value}`)}
              style={{ width: "98%" }}
            />
          </div>
        </div>
      </div>
    </Modal>
  );
}
