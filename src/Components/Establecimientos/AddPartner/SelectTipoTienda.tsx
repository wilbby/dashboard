import React from "react";
import { Select } from "antd";
import { useQuery } from "react-apollo";
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function TipoTienda(props: any) {
  const { handleChange, onSearchStore } = props;
  const { data, loading } = useQuery(query.TIPO_TIENDAS);

  const category = data && data.getTipoTienda ? data.getTipoTienda.data : [];

  return (
    <Select
      mode="multiple"
      allowClear
      style={{
        width: "50%",
      }}
      placeholder="Selecciona una o varios tipos de tienda"
      onChange={handleChange}
      onSearch={onSearchStore}
      loading={loading}
    >
      {category.map((item: any, i: number) => {
        return (
          <Option key={i} value={item._id}>
            {item.title}
          </Option>
        );
      })}
    </Select>
  );
}
