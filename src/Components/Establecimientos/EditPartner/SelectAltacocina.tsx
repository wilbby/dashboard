import React from "react";
import { Select } from "antd";
import { useQuery } from "react-apollo";
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function SelectCategory(props: any) {
  const { handleChange, onSearchStore, value } = props;
  const { data, loading } = useQuery(query.HighkitchenCategory);

  const category =
    data && data.getHighkitchenCategory ? data.getHighkitchenCategory.data : [];

  return (
    <Select
      mode="multiple"
      allowClear
      value={value}
      style={{
        width: "50%",
      }}
      placeholder="Selecciona una o varios tipos de comida"
      onChange={handleChange}
      onSearch={onSearchStore}
      loading={loading}
    >
      {category.map((item: any, i: number) => {
        return (
          <Option key={i} value={item._id}>
            {item.title}
          </Option>
        );
      })}
    </Select>
  );
}
