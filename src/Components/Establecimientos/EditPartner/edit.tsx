import React, { useState } from "react";
import "./add.css";
import {
  Modal,
  Tooltip,
  Upload,
  Input,
  InputNumber,
  Switch,
  message,
  Button,
  Select
} from "antd";
import {
  PlusCircleOutlined,
  LoadingOutlined,
  InstagramOutlined,
  FacebookOutlined,
  TwitterOutlined,
  GlobalOutlined,
} from "@ant-design/icons";
import { useMutation } from "react-apollo";
import { mutation } from "../../../GraphQL";
import SelectCategory from "./SelectCategory";
import SelectTipo from "./SelectTipo";
import SelectTipoTienda from "./SelectTipoTienda";
import SelectAltaCocina from "./SelectAltacocina";
import SelectCatName from "./SelectCategoryName";

const { Option } = Select;

const { TextArea } = Input;

const sheduledData = {
  Lunes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Martes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Miércoles: [
    {
      star: 9,
      end: 20,
    },
  ],

  Jueves: [
    {
      star: 9,
      end: 20,
    },
  ],

  Viernes: [
    {
      star: 9,
      end: 20,
    },
  ],

  Sábado: [
    {
      star: 9,
      end: 14,
    },
  ],

  Domingo: [],
};

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function EditPartner(props: any) {
  const { visible, setVisibleAdd, refetch, data, setDataStoreEdit, datosStripe } = props;


  const [imagen, setimagen] = useState(data.image);
  const [logo, setiLogo] = useState(data.logo);
  const [categoryID, setCategoryID] = useState(data.categoryID);
  const [categoryName, setCategoryName] = useState(data.categoryName);
  const [tipo, setTipo] = useState(data.tipo);
  const [loadingImage, setloadingImage] = useState(false);
  const [loadinglogo, setloadinglogo] = useState(false);
  const [city, setCity] = useState(data.city);
  const [lat, setlat] = useState(data.adress.lat);
  const [lgn, setlgn] = useState(data.adress.lgn);
  const [calle, setCalle] = useState(data.adress.calle);
  const [cp, setCp] = useState(data.adress.codigoPostal);

  const [title, setTitle] = useState(data.title);
  const [description, setDescription] = useState(data.description);
  const [shipping, setShipping] = useState(data.shipping);
  const [shippingLow, setShippingLow] = useState(data.previous_shipping);
  const [service, setService] = useState(data.extras);
  const [minime, setMinime] = useState(data.minime);
  const [email, setEmail] = useState(data.email);
  const [phone, setPhone] = useState(data.phone);
  const [stimated, setStimated] = useState(data.stimateTime);
  const [slug, setSlug] = useState(data.slug);
  const [tagOffert, setTagOffert] = useState(data.tagOffert);
  const [tipe, setTipe] = useState(data.type);
  const [isParnet, setisParnet] = useState(data.ispartners);
  const [isnew, setIsnew] = useState(data.isnew);
  const [open, setOpen] = useState(data.open);
  const [takeAway, setTakeawai] = useState(data.llevar);
  const [autoshipping, setAutoshipping] = useState(data.autoshipping);
  const [offert, setOffert] = useState(data.inOffert);
  const [cashPayment, setCashPayment] = useState(data.cashPayment);
  const [otter, setOtter] = useState(data.isOtterPartner);
  const [highkitchen, setHighkitchen] = useState(data.highkitchen);
  const [deliverect, setdeliverect] = useState(data.isDeliverectPartner);
  const [ordatic, setOrdatic] = useState(data.isOrdaticPartner);
  const [noScheduled, setNoscheduled] = useState(data.noScheduled);
  const [scheduleOnly, setScheduleOnly] = useState(data.scheduleOnly.available);
  const [scheduleOnlyHour, setScheduleOnlyHour] = useState(
    data.scheduleOnly.hour
  );
  const [collections, setcollections] = useState(data.collections);
  const [instagram, setinstagram] = useState(data.socialLink.instagram);
  const [facebook, setfacebook] = useState(data.socialLink.facebook);
  const [twitter, settwitter] = useState(data.socialLink.twitter);
  const [web, setweb] = useState(data.socialLink.web);
  const [numero, setNumero] = useState(data.adress.numero);
  const [Alergenos, setAlergenos] = useState(data.alegeno_url);
  const [forttalezaURL, setforttalezaURL] = useState(data.forttalezaURL);
  const [contactCode, setcontactCode] = useState(data.contactCode);

  const [inTop, setinTop] = useState(data.inTop);
  const [sort, setsort] = useState(data.sort);
  const [inHouse, setinHouse] = useState(data.inHouse);
  const [adsInHouse, setadsInHouse] = useState(data.adsInHouse);
  const [showAlergeno, setshowAlergeno] = useState(data.showAlergeno);

  const [openURL, setopenURL] = useState(data.openURL);
  const [stripeID, setstripeID] = useState(data.stripeID);
  const [url, seturl] = useState(data.url);
  const [aceptReservation, setaceptReservation] = useState(
    data.aceptReservation
  );

  const [singleUploadToStoreImagenAws] = useMutation(
    mutation.UPLOAD_FILE_STORE
  );

  const [actualizarRestaurantAdmin] = useMutation(mutation.UPDATE_STORE);

  const input = {
    _id: data._id,
    title: title,
    image: imagen,
    description: description,
    shipping: shipping,
    extras: service,
    minime: minime,
    rating: data.rating,
    diaslaborales: data.diaslaborales,
    categoryName: categoryName,
    categoryID: categoryID,
    phone: phone,
    email: email,
    logo: logo,
    type: tipe,
    tipo: tipo,
    ispartners: isParnet,
    isnew: isnew,
    llevar: takeAway,
    alegeno_url: Alergenos,
    autoshipping: autoshipping,
    open: open,
    previous_shipping: shippingLow,
    stimateTime: stimated,
    slug: slug,
    highkitchen: highkitchen,
    inOffert: offert,
    includeCity: [city],
    OnesignalID: data.OnesignalID,
    channelLinkId: data.channelLinkId,
    collections: collections,
    isDeliverectPartner: deliverect,
    salvingPack: data.salvingPack,
    isOrdaticPartner: ordatic,
    isOrdaticPartnerID: data.isOrdaticPartnerID,
    adress: {
      calle: calle,
      numero: numero,
      codigoPostal: cp,
      ciudad: city,
      lat: lat,
      lgn: lgn,
    },
    city: city,
    contactCode: contactCode,
    socialLink: {
      instagram: instagram,
      facebook: facebook,
      twitter: twitter,
      web: web,
    },
    isOtterPartner: otter,
    OtterPartnerId: data.OtterPartnerId,
    noScheduled: noScheduled,
    scheduleOnly: {
      available: scheduleOnly,
      hour: scheduleOnlyHour,
    },
    shorting: data.shorting,
    cashPayment: cashPayment,
    tagOffert: tagOffert ? tagOffert : null,
    forttalezaURL: forttalezaURL,
    openURL: openURL,
    url: url,
    aceptReservation: aceptReservation,
    reservationScheduled: data.reservationScheduled
      ? data.reservationScheduled
      : sheduledData,
    inTop: inTop,
    sort: sort,
    inHouse: inHouse,
    adsInHouse: adsInHouse,
    showAlergeno: showAlergeno,
    stripeID: stripeID
  };

  const handleCancel = () => {
    setDataStoreEdit(null);

    setTimeout(() => {
      setVisibleAdd(false);
    }, 500);
  };

  function handleChange(value: any) {
    setCategoryID(value);
  }

  function handleChangeCatName(value: any) {
    setCategoryName(value);
  }

  function handleChangeTipe(value: any) {
    setTipo(value);
  }

  const createdStore = () => {
    actualizarRestaurantAdmin({ variables: { input: { data: input } } })
      .then((res: any) => {
        const result = res.data.actualizarRestaurantAdmin;
        if (result.success) {
          message.success(result.messages);
          refetch();
        } else {
          message.warning(result.messages);
        }
      })
      .catch(() => {
        message.error("Algo salio mal intentalo de nuevo");
      });
  };
  const uploadButton = (
    <Tooltip title="Añadir foto de portada">
      <div
        style={{
          width: 450,
          height: 200,
          borderRadius: 10,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadingImage ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        )}

        <div className="ant-upload-text">
          <span style={{ fontSize: 12 }}>
            Añadir foto de portada 450px X 200px
          </span>
        </div>
      </div>
    </Tooltip>
  );

  const uploadButtonAvatar = (
    <Tooltip title="Añadir logo del establecimiento">
      <div
        style={{
          width: 100,
          height: 100,
          borderRadius: 10,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {loadinglogo ? (
          <LoadingOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        ) : (
          <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        )}

        <div className="ant-upload-text">
          <span style={{ fontSize: 12 }}>Añadir logo 150px X 150px</span>
        </div>
      </div>
    </Tooltip>
  );

  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      okButtonProps={{ disabled: lat && lgn ? false : true }}
      onOk={createdStore}
      okText="Actualizar establecimiento"
      cancelText="Cancelar"
      width={700}
    >
      <div style={{ minHeight: 700, padding: 20 }}>
        {
          //@ts-ignore
          <h4>{`Añadir establecimiento`}</h4>
        }

        <div>
          <div
            style={{
              width: 450,
              height: 200,
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: 30,
            }}
          >
            <Upload
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              customRequest={async (data) => {
                setloadingImage(true);
                let file = await getBase64(data.file);
                singleUploadToStoreImagenAws({
                  variables: { file },
                })
                  .then((res: any) => {
                    setloadingImage(false);
                    setimagen(
                      res.data.singleUploadToStoreImagenAws.data.Location
                    );
                  })
                  .catch((error: any) => {
                    setloadingImage(false);
                    message.error(
                      "Imagen muy grande reduce el tamaño de la misma"
                    );
                  });
              }}
            >
              {imagen ? (
                <Tooltip title="Haz click para cambiar">
                  <img className="imagen_prod_add" src={imagen} />
                </Tooltip>
              ) : null}

              {!imagen ? uploadButton : null}
            </Upload>
          </div>

          <div
            style={{
              width: 100,
              height: 100,
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: -40,
            }}
          >
            <Upload
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              customRequest={async (data) => {
                setloadinglogo(true);
                let file = await getBase64(data.file);

                singleUploadToStoreImagenAws({ variables: { file } })
                  .then((res: any) => {
                    setloadinglogo(false);
                    setiLogo(
                      res.data.singleUploadToStoreImagenAws.data.Location
                    );
                  })
                  .catch((error: any) => {
                    setloadinglogo(false);
                    message.error(
                      "Imagen muy grande reduce el tamaño de la misma"
                    );
                  });
              }}
            >
              {logo ? (
                <Tooltip title="Haz click para cambiar">
                  <img className="imagen_prod_add_logo" src={logo} />
                </Tooltip>
              ) : null}

              {!logo ? uploadButtonAvatar : null}
            </Upload>
          </div>
        </div>

        <div
          style={{
            textAlign: "center",
            marginTop: 30,
          }}
        >
          <Button
            type="primary"
            href="https://www.iloveimg.com/es/comprimir-imagen"
            target="_blank"
          >
            Comprimir imagen
          </Button>
        </div>

        <div className="Forma" style={{ marginTop: 30 }}>
          <Input
            placeholder="Nombre el establecomiento"
            onChange={(e) => setTitle(e.target.value)}
            value={title}
          />
          <TextArea
            placeholder="Descripción"
            rows={4}
            style={{ marginTop: 15, height: 120 }}
            value={description}
            showCount
            maxLength={150}
            onChange={(e) => setDescription(e.target.value)}
          />
          <div className="Forma_prices">
            <div style={{ marginTop: 10 }}>
              <span>Envío</span>
              <br />
              <InputNumber
                type="number"
                placeholder="Precio de envío"
                value={shipping / 100}
                style={{ width: 130 }}
                min={0}
                onChange={(e) => setShipping(Number(e) * 100)}
              />
            </div>
            <div style={{ marginTop: 10 }}>
              <span>Envío rebajado</span>
              <br />
              <InputNumber
                type="number"
                placeholder="Envío rebajado"
                value={shippingLow / 100}
                style={{ width: 130 }}
                min={0}
                onChange={(e) => setShippingLow(Number(e) * 100)}
              />
            </div>
            <div style={{ marginTop: 10 }}>
              <span>Servicio</span>
              <br />
              <InputNumber
                type="number"
                placeholder="Tarifa servicio"
                value={service / 100}
                style={{ width: 130 }}
                min={0}
                onChange={(e) => setService(Number(e) * 100)}
              />
            </div>
            <div style={{ marginTop: 10 }}>
              <span>Minímo</span>
              <br />
              <InputNumber
                type="number"
                placeholder="Minímo"
                value={minime / 100}
                style={{ width: 170 }}
                min={0}
                onChange={(e) => setMinime(Number(e) * 100)}
              />
            </div>
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Email"
              type="email"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            />
            <Input
              placeholder="Teléfono"
              type="tel"
              style={{ width: "50%" }}
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Tiempo estimado (35-45 min)"
              type="name"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setStimated(e.target.value)}
              value={stimated}
            />
            <Input
              placeholder="Slug Elemplo (el-manantial)"
              type="name"
              style={{ width: "50%" }}
              onChange={(e) => setSlug(e.target.value)}
              value={slug}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <Input
              placeholder="Ofertas o Promociones (50% Off)"
              type="name"
              style={{ width: "45%", marginRight: 15 }}
              onChange={(e) => setTagOffert(e.target.value)}
              value={tagOffert}
            />
            <Input
              placeholder="Tipo Ejm: (Asiática | Sushi)"
              type="name"
              style={{ width: "50%" }}
              value={tipe}
              onChange={(e) => setTipe(e.target.value)}
            />
          </div>

          <div style={{ marginTop: 20, flexDirection: "row" }}>
            <SelectCategory handleChange={handleChange} value={categoryID} />
            <br />
            <SelectCatName
              handleChange={handleChangeCatName}
              value={categoryName}
            />
            {categoryID.filter((x: any) => x === "5fb7a32cb234a46c09297804")
              .length > 0 ? (
              <>
                {highkitchen ? (
                  <SelectAltaCocina
                    handleChange={handleChangeTipe}
                    value={tipo}
                  />
                ) : (
                  <SelectTipo handleChange={handleChangeTipe} value={tipo} />
                )}
              </>
            ) : null}
            {categoryID.filter((x: any) => x === "5fb7aec4b234a46c0929780b")
              .length > 0 ? (
              <SelectTipoTienda handleChange={handleChangeTipe} value={tipo} />
            ) : null}
          </div>

          <div className="boolean_content">
            <div>
              <br />
              <Switch
                checkedChildren="Partner Asocioado"
                unCheckedChildren="Partner Asocioado"
                defaultChecked={isParnet}
                onChange={(checked) => setisParnet(checked)}
              />
              <br />
              <Switch
                checkedChildren="Takeaway"
                unCheckedChildren="Takeaway"
                defaultChecked={takeAway}
                style={{ marginTop: 20 }}
                onChange={(checked) => setTakeawai(checked)}
              />
              <br />
              <Switch
                checkedChildren="Destacado"
                unCheckedChildren="Destacado"
                defaultChecked={offert}
                style={{ marginTop: 20 }}
                onChange={(checked) => setOffert(checked)}
              />
              <br />
              <Switch
                checkedChildren="Otter Partner"
                unCheckedChildren="Otter Partner"
                defaultChecked={otter}
                style={{ marginTop: 20 }}
                onChange={(checked) => setOtter(checked)}
              />
            </div>
            <div>
              <Switch
                checkedChildren="Nuevo"
                unCheckedChildren="Nuevo"
                defaultChecked={isnew}
                onChange={(checked) => setIsnew(checked)}
              />
              <br />
              <Switch
                checkedChildren="Entrega por el partner"
                unCheckedChildren="Entrega por el partner"
                defaultChecked={autoshipping}
                style={{ marginTop: 20 }}
                onChange={(checked) => setAutoshipping(checked)}
              />
              <br />
              <Switch
                checkedChildren="No aceptar programado"
                unCheckedChildren="No aceptar programado"
                disabled={scheduleOnly}
                defaultChecked={noScheduled}
                style={{ marginTop: 20 }}
                onChange={(checked) => setNoscheduled(checked)}
              />
              <br />
              <Switch
                checkedChildren="Deliverect Partner"
                unCheckedChildren="Deliverect Partner"
                defaultChecked={deliverect}
                style={{ marginTop: 20 }}
                onChange={(checked) => setdeliverect(checked)}
              />
            </div>
            <div>
              <Switch
                checkedChildren="Abierto"
                unCheckedChildren="Cerrado"
                defaultChecked={open}
                onChange={(checked) => setOpen(checked)}
              />
              <br />
              <Switch
                checkedChildren="Alta Cocina"
                unCheckedChildren="Alta Cocina"
                defaultChecked={highkitchen}
                style={{ marginTop: 20 }}
                onChange={(checked) => setHighkitchen(checked)}
              />
              <br />
              <Switch
                checkedChildren="Pago en efectivo"
                unCheckedChildren="Pago en efectivo"
                defaultChecked={cashPayment}
                onChange={(checked) => setCashPayment(checked)}
                style={{ marginTop: 20 }}
              />
              <br />
              <Switch
                checkedChildren="Ordatic Partner"
                unCheckedChildren="Ordatic Partner"
                defaultChecked={ordatic}
                onChange={(checked) => setOrdatic(checked)}
                style={{ marginTop: 20 }}
              />
            </div>
          </div>
          <div style={{ marginTop: 30, flexDirection: "row" }}>
            <div>
              <Switch
                checkedChildren="Sólo programado"
                unCheckedChildren="Sólo programado"
                defaultChecked={scheduleOnly}
                disabled={noScheduled}
                onChange={(checked) => {
                  setScheduleOnly(checked);
                  setNoscheduled(false);
                }}
              />

              <InputNumber
                type="number"
                disabled={scheduleOnly ? false : true}
                placeholder="Horas de antelación"
                value={scheduleOnlyHour}
                style={{ width: 170, marginLeft: 15, marginTop: 10 }}
                onChange={(e) => setScheduleOnlyHour(Number(e))}
                min={0}
              />
            </div>
            <div style={{ marginTop: 30, display: "flex" }}>
              <Switch
                checkedChildren="Abrir web"
                unCheckedChildren="Abrir detalle"
                defaultChecked={openURL}
                onChange={(checked) => setopenURL(checked)}
              />
              <br />
              <Switch
                checkedChildren="Aceptar reservas"
                unCheckedChildren="No aceptar reservas"
                defaultChecked={aceptReservation}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setaceptReservation(checked)}
              />
            </div>
            <h3 style={{ marginTop: 30 }}>Cartas digitales</h3> <br />
            <div style={{ display: "flex" }}>
              <Switch
                checkedChildren="Carta digital"
                unCheckedChildren="Carta digital disponible"
                defaultChecked={inHouse}
                onChange={(checked) => setinHouse(checked)}
              />
              <br />
              <Switch
                checkedChildren="Anuncios en la carta"
                unCheckedChildren="Anuncios en la carta disponible"
                defaultChecked={adsInHouse}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setadsInHouse(checked)}
              />
              <br />
              <Switch
                checkedChildren="Mostrar alérgenos"
                unCheckedChildren="Mostrar alérgenos"
                defaultChecked={showAlergeno}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setshowAlergeno(checked)}
              />
            </div>
            <h3 style={{ marginTop: 30 }}>Lista de Tops</h3> <br />
            <div>
              <Switch
                checkedChildren="Mostrar en Tops"
                unCheckedChildren="Montrando en Tops"
                defaultChecked={inTop}
                onChange={(checked) => setinTop(checked)}
              />

              {inTop ? (
                <InputNumber
                  type="number"
                  placeholder="Posición"
                  value={sort}
                  style={{ width: 170, marginTop: 10, marginLeft: 10 }}
                  onChange={(e) => setsort(Number(e))}
                  min={0}
                />
              ) : null}
            </div>
            <div style={{ marginTop: 30 }}>
              <h3>Mostrar los productos como:</h3>
              <Switch
                checkedChildren="Colecciones"
                unCheckedChildren="Lista"
                defaultChecked={collections}
                onChange={(checked) => setcollections(checked)}
              />
            </div>
            <div style={{ marginTop: 30, display: "flex" }}>
              <Switch
                checkedChildren="Abrir web"
                unCheckedChildren="Abrir detalle"
                defaultChecked={openURL}
                onChange={(checked) => setopenURL(checked)}
              />
              <br />
              <Switch
                checkedChildren="Aceptar reservas"
                unCheckedChildren="No aceptar reservas"
                defaultChecked={aceptReservation}
                style={{ marginLeft: 10 }}
                onChange={(checked) => setaceptReservation(checked)}
              />
            </div>
          </div>

          <div style={{ marginTop: 20 }}>
            <h3>Dirección del establecomiento</h3>

            <Input
              placeholder="Dirección del establecimiento"
              onChange={(e) => setCalle(e.target.value)}
              style={{ width: "100%", marginTop: 30 }}
              value={calle}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 30,
              }}
            >
              <div style={{ flexDirection: "row" }}>
                <Input
                  placeholder="Número"
                  onChange={(e) => setNumero(e.target.value)}
                  style={{ width: 180, marginRight: 15 }}
                  value={numero}
                />
                <Input
                  placeholder="Código postal"
                  value={cp}
                  onChange={(e) => setCp(e.target.value)}
                  style={{ width: 180, marginRight: 15 }}
                />
                <Input
                  placeholder="Ciudad"
                  onChange={(e) => setCity(e.target.value)}
                  value={city}
                  style={{ width: 265, marginTop: 20 }}
                />
              </div>
            </div>

            <div
              style={{
                marginTop: 20,
                width: 700,
              }}
            >
              <div style={{ flexDirection: "row" }}>
                <Input
                  placeholder="Latitud (42.3439925)"
                  onChange={(e) => setlat(e.target.value)}
                  style={{ width: "45%", marginRight: 15 }}
                  value={lat}
                />
                <Input
                  placeholder="Longitud (-3.696906)"
                  value={lgn}
                  onChange={(e) => setlgn(e.target.value)}
                  style={{ width: "40%", marginRight: 15 }}
                />
              </div>

              <Input
                value={contactCode}
                placeholder="CIF | DNI o NIE del propietario"
                onChange={(e) => setcontactCode(`${e.target.value}`)}
                style={{ width: "87%", marginTop: 15 }}
              />

             <Select
                showSearch
                style={{ width: "87%", marginTop: 15 }}
                placeholder="Selecciona cuenta de Stripe Connect"
                optionFilterProp="children"
                defaultValue={stripeID}
                allowClear
                onChange={(value: string)=> setstripeID(value)}
                filterOption={(input: any, option: any) =>
                  //@ts-ignore
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                 {datosStripe.map((data: any, i: any) => {
                  return (
                    <Option value={data.id} key={i}>
                     {data.company.name}
                    </Option>
                  );
                })} 
              </Select>
            </div>
          </div>

          <div style={{ marginTop: 30, flexDirection: "row" }}>
            <Input
              value={instagram}
              prefix={<InstagramOutlined />}
              placeholder="URL instagram"
              style={{ marginBottom: 15, width: "45%", marginRight: 15 }}
              onChange={(e) => setinstagram(e.target.value)}
            />
            <Input
              value={facebook}
              prefix={<FacebookOutlined />}
              placeholder="URL facebook"
              style={{ marginBottom: 15, width: "45%" }}
              onChange={(e) => setfacebook(e.target.value)}
            />
            <Input
              value={twitter}
              prefix={<TwitterOutlined />}
              placeholder="URL twitter"
              style={{ marginBottom: 15, width: "45%", marginRight: 15 }}
              onChange={(e) => settwitter(e.target.value)}
            />
            <Input
              value={web}
              prefix={<GlobalOutlined />}
              placeholder="Sitio web"
              onChange={(e) => setweb(e.target.value)}
              style={{ marginBottom: 15, width: "45%" }}
            />

            <Input
              value={Alergenos}
              placeholder="URL de Alergeno del establecimiento"
              onChange={(e) => setAlergenos(`${e.target.value}`)}
              style={{ marginBottom: 15, width: "45%", marginRight: 15 }}
            />

            <Input
              value={forttalezaURL}
              placeholder="URL de perfil Forttalezza"
              onChange={(e) => setforttalezaURL(`${e.target.value}`)}
              style={{ width: "45%" }}
            />

            <Input
              value={url}
              placeholder="URL de detalle"
              onChange={(e) => seturl(`${e.target.value}`)}
              style={{ width: "92%" }}
            />
          </div>
        </div>
      </div>
    </Modal>
  );
}
