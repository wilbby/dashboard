import React, { useState } from "react";
import { Select } from "antd";
import { useQuery } from "react-apollo";
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function Stores(props: any) {
  const [search, setsearch] = useState("");
  const { onChangeStore, city } = props;

  const { data, loading } = useQuery(query.RESTAURANT_EDIT, {
    variables: {
      city: city,
      search: search,
      page: 1,
      limit: 20,
    },
  });

  const stores =
    data && data.getRestaurantforEdit ? data.getRestaurantforEdit.data : [];

  const onSearch = (val: any) => {
    setsearch(val);
  };

  return (
    <Select
      showSearch
      style={{ width: 360, marginTop: 20 }}
      placeholder="Selectcciona un establecimiento"
      optionFilterProp="children"
      allowClear
      loading={loading}
      onChange={onChangeStore}
      onSearch={onSearch}
      filterOption={(input, option) =>
        //@ts-ignore
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <>
        {stores &&
          stores.map((datos: any, i: any) => {
            return (
              <Option value={datos._id} key={i}>
                {datos.title}
              </Option>
            );
          })}
      </>
    </Select>
  );
}
