import React, { useState } from "react";
import "./index.css";
import dataMunicipio from "../../Orders/municipio.json";
import { Input, Select, Rate, Button, message } from "antd";
import SelecUser from "./customer";
import Stores from "./stores";
import { mutation } from "../../../GraphQL";
import { useMutation } from "react-apollo";

const { Option } = Select;
const { TextArea } = Input;

export default function AddRating() {
  const [city, setcity] = useState("Tomelloso");
  const [store, setstore] = useState("");
  const [user, setuser] = useState("");
  const [coments, setcoments] = useState(null);
  const [rating, setrating] = useState(0);

  const [crearValoracion] = useMutation(mutation.CREAR_COMMENT);

  function onChange(value) {
    setcity(value);
  }

  function onChangeStore(value) {
    setstore(value);
  }

  function onChangeUser(value) {
    setuser(value);
  }

  const input = {
    user: user,
    comment: coments,
    value: rating,
    restaurant: store,
  };

  const createValoracion = () => {
    if (rating === 0) {
      message.warning("Debes añadir una valoración");
    } else if (!user) {
      message.warning("Debes seleccionar un usuario");
    } else if (!store) {
      message.warning("Debes seleccionar un establecimiento");
    } else {
      crearValoracion({ variables: { input } })
        .then((res) => {
          const da = res.data.crearValoracion;
          if (da.id) {
            message.success("Valoración añadida con exito");
            setrating(0);
            setuser("");
            setstore("");
            setcoments(null);
          } else {
            message.warning("Algo salio mal intentalo de nuevo");
          }
        })
        .catch((e) => {
          console.error(e);
          message.error("Algo salio mal intentalo de nuevo");
        });
    }
  };

  const isAdd = () => {
    if (rating === 0) {
      return true;
    } else if (!user) {
      return true;
    } else if (!store) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <div className="rating_content">
      <div className="rating_content_form">
        <Select
          showSearch
          style={{ width: 360 }}
          placeholder="Selectcciona una ciudad"
          optionFilterProp="children"
          defaultValue={city}
          allowClear
          onChange={onChange}
          filterOption={(input, option) =>
            //@ts-ignore
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {dataMunicipio.map((data, i) => {
            return (
              <Option value={data.nombre} key={i}>
                {data.nombre}
              </Option>
            );
          })}
        </Select>

        <Stores onChangeStore={onChangeStore} city={city} />

        <SelecUser onChangeUser={onChangeUser} />

        <div style={{ marginTop: 20 }}>
          <p>Añade una valoración</p>
          <Rate
            style={{ fontSize: 25 }}
            value={rating}
            onChange={(value) => setrating(value)}
          />
        </div>

        <div style={{ marginTop: 20 }}>
          <p>Añade un comentario (opcional)</p>
          <TextArea
            rows={4}
            allowClear={true}
            onChange={(e) => setcoments(e.target.value)}
          />
        </div>

        <div style={{ marginTop: 20 }}>
          <Button
            type="primary"
            onClick={createValoracion}
            disabled={isAdd()}
            style={{ borderRadius: 10, width: "100%", height: 50 }}
          >
            Añadir valoración
          </Button>
        </div>
      </div>
    </div>
  );
}
