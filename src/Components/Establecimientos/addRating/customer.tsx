import React from "react";
import { Select } from "antd";
import { useQuery } from "react-apollo";
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function Customer(props: any) {
  const { onChangeUser } = props;

  function getRandomArbitrary(min: number, max: number) {
    return Math.random() * (max - min) + min;
  }

  const { data, loading } = useQuery(query.CUSTOMERS, {
    variables: {
      page: Math.round(getRandomArbitrary(1, 10)),
      limit: 40,
      email: null,
    },
  });

  const customers = data && data.getCustomers ? data.getCustomers.data : [];

  return (
    <Select
      showSearch
      style={{ width: 360, marginTop: 20 }}
      placeholder="Selectcciona un cliente"
      optionFilterProp="children"
      allowClear
      loading={loading}
      onChange={onChangeUser}
      filterOption={(input, option) =>
        //@ts-ignore
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <>
        {customers &&
          customers.map((customer: any, i: any) => {
            return (
              <Option value={customer._id} key={i}>
                {customer.name} {customer.lastName}
              </Option>
            );
          })}
      </>
    </Select>
  );
}
