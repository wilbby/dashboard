import React, { useState, useEffect } from "react";
import {
  List,
  Avatar,
  Input,
  Select,
  Tag,
  Switch,
  Button,
  message,
  Skeleton,
} from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";
import dataMunicipio from "../Orders/municipio.json";
import { useQuery, useMutation } from "react-apollo";
import { query, mutation } from "../../GraphQL";
import ModalHour from "./FormHorario";
import AddPartner from "./AddPartner/add";
import EditPartner from "./EditPartner/edit";
import { LOCAL_API_URL } from "../../Config";

const { Search } = Input;
const { Option } = Select;

export default function Establecimiento() {
  const [city, setcity] = useState("Tomelloso");
  const [search, setsearch] = useState("");
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(10);
  const [dataStore, setdataStore] = useState(null);
  const [dataStoreEdit, setDataStoreEdit] = useState(null);
  const [visible, setvisible] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [itemID, setitemID] = useState(null);
  const [datosStripe, setDatos] = useState([]);

  const [visibleAdd, setvisibleAdd] = useState(false);
  const [visibleEdit, setvisibleedit] = useState(false);

  const [actualizarRestaurant] = useMutation(mutation.ACTUALIZAR_STORE);


  const getDatos = async () => {
    setLoading(true);
    const resp = await fetch(`${LOCAL_API_URL}/stripe-connect`, {
      method: "GET",
    });
    const response = await resp.json();
    if (response.success) {
      setLoading(false);
      setDatos(response.data);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    getDatos();
  }, []);

  const { data, loading, refetch } = useQuery(query.RESTAURANT_EDIT, {
    variables: { city: city, search: search, page: page, limit: limit },
  });

  const listData =
    data && data.getRestaurantforEdit ? data.getRestaurantforEdit.data : [];

  function onChange(value: string) {
    setcity(value);
    setpage(1);
  }

  const onSearch = (search: string) => {
    setsearch(search);
    setpage(1);
  };

  return (
    <div>
      {loading ? (
        <>
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
          <Skeleton avatar title={false} loading={loading} active />
        </>
      ) : (
        <List
          className="demo-loadmore-list"
          pagination={{
            onChange: (page) => {
              setpage(page);
            },
            pageSize: limit,
            total: 50000,
          }}
          dataSource={listData}
          header={
            <div>
              <div className="search_container">
                <div>
                  <h3>Filtros</h3>
                  <Select
                    showSearch
                    style={{ width: 300 }}
                    placeholder="Selectcciona una ciudad"
                    optionFilterProp="children"
                    defaultValue={city}
                    allowClear
                    onChange={onChange}
                    filterOption={(input, option) =>
                      //@ts-ignore
                      option.children
                        //@ts-ignore
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {dataMunicipio.map((data, i) => {
                      return (
                        <Option value={data.nombre} key={i}>
                          {data.nombre}
                        </Option>
                      );
                    })}
                  </Select>
                  <br />
                  <Button
                    type="primary"
                    style={{ marginTop: 20, height: 45 }}
                    icon={<PlusCircleOutlined />}
                    onClick={() => setvisibleAdd(true)}
                  >
                    Añadir establecimiento
                  </Button>
                </div>
                <div>
                  <Search
                    style={{ width: "100%", minWidth: 350 }}
                    placeholder="Buscar establecimiento"
                    allowClear
                    enterButton="Buscar"
                    size="large"
                    onSearch={onSearch}
                  />
                </div>
              </div>
            </div>
          }
          renderItem={(item: any) => (
            <List.Item
              actions={[
                <Button
                  type="link"
                  onClick={() => {
                    setDataStoreEdit(item);
                    setvisibleedit(true);
                  }}
                >
                  Editar
                </Button>,
                <Button
                  type="link"
                  onClick={() => {
                    setdataStore(item);
                    setvisible(true);
                  }}
                >
                  Ver horario
                </Button>,
              ]}
            >
              <List.Item.Meta
                avatar={
                  <Avatar
                    src={item.image}
                    shape="square"
                    size={100}
                    style={{ borderRadius: 10 }}
                  />
                }
                description={
                  <>
                    {item.adress ? (
                      <p>
                        {item.adress.calle}, {item.adress.numero},{" "}
                        {item.adress.codigoPostal}, {item.adress.ciudad}
                      </p>
                    ) : null}
                    Email: {item.email} <br />
                    <span
                      style={{
                        fontWeight: "bold",
                        color: item.ispartners ? "#52c41a" : "red",
                      }}
                    >
                      {item.ispartners
                        ? "Partner asociado"
                        : "Partner no asociado"}
                    </span>{" "}
                    - <span>Rating: {item.rating}</span>
                    <br />
                  </>
                }
                title={
                  <h1 style={{ fontWeight: "bold" }}>
                    {item.title} -{" "}
                    {item.open ? (
                      <Tag color="green">
                        <span>Abierto</span>
                      </Tag>
                    ) : (
                      <Tag color="red">
                        <span>Cerrado</span>
                      </Tag>
                    )}
                  </h1>
                }
              />
              <div
                style={{ justifyContent: "flex-end", alignItems: "flex-end" }}
              >
                <Switch
                  checked={item.open}
                  loading={Loading && itemID === item._id}
                  style={{ marginBottom: 15 }}
                  onChange={(value) => {
                    setLoading(true);
                    setitemID(item._id);
                    actualizarRestaurant({
                      variables: {
                        input: {
                          _id: item._id,
                          open: value,
                        },
                      },
                    })
                      .then((res) => {
                        if (res.data.actualizarRestaurant.success) {
                          refetch();
                          message.success("Establecimiento actualizado");
                          setLoading(false);
                          setitemID(null);
                        } else {
                          message.warning("Algo salio mal intentalo de nuevo");
                          setLoading(false);
                          setitemID(null);
                          refetch();
                        }
                      })
                      .catch(() => {
                        message.error("Error del sistema");
                        setLoading(false);
                        setitemID(null);
                      });
                  }}
                  checkedChildren="Abierto"
                  unCheckedChildren="Cerrado"
                />
                <br />
                <Switch
                  checked={item.inOffert}
                  loading={Loading && itemID === item._id}
                  onChange={(value) => {
                    setLoading(true);
                    setitemID(item._id);
                    actualizarRestaurant({
                      variables: {
                        input: {
                          _id: item._id,
                          inOffert: value,
                        },
                      },
                    })
                      .then((res) => {
                        if (res.data.actualizarRestaurant.success) {
                          if (refetch) {
                            refetch();
                          }
                          message.success("Establecimiento actualizado");
                          setLoading(false);
                          setitemID(null);
                        } else {
                          message.warning("Algo salio mal intentalo de nuevo");
                          setLoading(false);
                          setitemID(null);
                          if (refetch) {
                            refetch();
                          }
                        }
                      })
                      .catch((e) => {
                        message.error("Error del sistema");
                        setLoading(false);
                        setitemID(null);
                        console.error(e);
                      });
                  }}
                  checkedChildren="Destacado"
                  unCheckedChildren="No destacado"
                />
              </div>
            </List.Item>
          )}
        />
      )}

      {dataStore ? (
        <ModalHour
          data={dataStore}
          setvisible={setvisible}
          setdataStore={setdataStore}
          visible={visible}
          actualizarRestaurant={actualizarRestaurant}
          refetch={refetch}
        />
      ) : null}

      <AddPartner
        visible={visibleAdd}
        setVisibleAdd={setvisibleAdd}
        refetch={refetch}
      />
      {dataStoreEdit ? (
        <EditPartner
          data={dataStoreEdit}
          visible={visibleEdit}
          setVisibleAdd={setvisibleedit}
          refetch={refetch}
          setDataStoreEdit={setDataStoreEdit}
          datosStripe={datosStripe}
        />
      ) : null}
    </div>
  );
}
