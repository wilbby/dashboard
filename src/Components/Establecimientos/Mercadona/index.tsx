import React, {useState} from 'react'
import {Button, Input, message, Select} from "antd";
import { useQuery } from 'react-apollo';
import { query } from "../../../GraphQL";

const { Option } = Select;

export default function Mercadona() {

    const [mercadonaId, setMercadonaID] = useState("")
    const [wilbbyId, setWilbbyId] = useState("")

    const {data = {}, loading, refetch} = useQuery(query.GET_SUBCOLLECTION)
    const {getSubCollectionAdmin} = data

    const datos = getSubCollectionAdmin ? getSubCollectionAdmin.data : []


    const isAdd = () => {
        if (mercadonaId && wilbbyId) {
          return false;
        } else {
          return true;
        }
    }

    const onChange = (value: any) => {
        setWilbbyId(value)
    }


    const saveProduct = () => {
        fetch(`https://api.v2.wilbby.com/create-product-mercadona?id=${mercadonaId}&cat=${wilbbyId}`).then(()=> {
            message.success("Productos guradado con exito")
            setMercadonaID("")
            setWilbbyId("")
        }).catch(()=> {
            message.error("Algo salio mal intentalo de nuevo")
        })
    }
  return (
    <div className="rating_content">
      <div className="rating_content_form">
      <div>
      <p>Categoria de Wilbby</p>
       <Select
          showSearch
          style={{ width: 360 }}
          placeholder="Selectcciona una ciudad"
          optionFilterProp="children"
          defaultValue={wilbbyId}
          allowClear
          onChange={onChange}
          filterOption={(input: any, option: any) =>
            //@ts-ignore
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {datos.map((data: any, i: any) => {
            return (
              <Option value={data._id} key={i}>
                {data.title}
              </Option>
            );
          })}
        </Select>
      </div>
       

        <div style={{ marginTop: 20 }}>
          <p>Id de mercadona</p>
          <Input onChange={(e) => setMercadonaID(e.target.value)} placeholder="Mercadona ID"/>
        </div>

        <div style={{ marginTop: 20 }}>
          <Button
            type="primary"
            onClick={saveProduct}
            disabled={isAdd()}
            style={{ borderRadius: 10, width: "100%", height: 50 }}
          >
            Crear productos
          </Button>
        </div>
      </div>
    </div>
  )
}
