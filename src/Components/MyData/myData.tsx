import React, { useState } from "react";
import Portada from "../../Assets/image/portada.png";
import { mutation } from "../../GraphQL";
import { Mutation, useMutation } from "react-apollo";
import { Avatar, Tooltip, Upload, Input, Form, message } from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";
import { IMAGES_PATH } from "../../Config";
import "./index.css";

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
export default function MyData(props: any) {
  const { user, refetch, setvisible } = props;
  const [avatar, setAvatar] = useState(user.avatar);
  const [name, setName] = useState(user.name);
  const [lastName, setlastName] = useState(user.lastName);
  const [city, setcity] = useState(user.city);
  const [telefono, settelefono] = useState(user.telefono);
  const [email, setemail] = useState(user.email);
  const [actualizarAdmin] = useMutation(mutation.ACTUALIZAR_ADMIN);

  const [form] = Form.useForm();

  const updateDate = (value: any) => {
    setName(value.name);
    setlastName(value.lastName);
    setcity(value.city);
    settelefono(value.telefono);
    setemail(value.email);
    const input = {
      _id: user._id,
      name: name,
      lastName: lastName,
      city: city,
      email: email,
      avatar: avatar,
      telefono: telefono,
    };
    actualizarAdmin({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarAdmin) {
          message.success("Datos actualizado");
          setvisible(false);
        } else {
          message.error("Algo va mal intentalo de nuevo");
        }
      })
      .catch(() => {
        message.error("Algo va mal intentalo de nuevo");
      });
  };

  const uploadButton = (
    <Tooltip title="Añadir foto del perfil">
      <div
        style={{
          width: 150,
          height: 150,
          borderRadius: 100,
          alignItems: "center",
          justifyContent: "center",
          padding: 10,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <PlusCircleOutlined style={{ fontSize: 30, color: "#90c33c" }} />
        <div className="ant-upload-text" style={{ marginTop: 10 }}>
          Añadir foto del perfil
        </div>
      </div>
    </Tooltip>
  );

  return (
    <div className="modal_content">
      <div className="portada">
        <img src={Portada} alt="Wilbby" />
        <div className="avatar_cont">
          <Mutation mutation={mutation.UPLOAD_FILE}>
            {(singleUpload: any) => (
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                customRequest={async (data) => {
                  let imgBlob = await getBase64(data.file);
                  singleUpload({ variables: { imgBlob } })
                    .then((res: any) => {
                      setAvatar(
                        res && res.data && res.data.singleUpload
                          ? res.data.singleUpload.filename
                          : ""
                      );

                      const input = {
                        _id: user._id,
                        name: user.name,
                        lastName: user.lastName,
                        city: user.city,
                        email: user.email,
                        avatar:
                          res && res.data && res.data.singleUpload
                            ? res.data.singleUpload.filename
                            : "",
                        telefono: user.telefono,
                      };
                      actualizarAdmin({
                        variables: {
                          input: input,
                        },
                      });

                      refetch();
                    })
                    .catch((error: any) => {
                      console.log("fs error: ", error);
                    });
                }}
              >
                {avatar ? (
                  <Tooltip title="Actualizar foto del perfil">
                    <Avatar src={IMAGES_PATH + avatar} size={150} />
                  </Tooltip>
                ) : null}

                {!avatar ? uploadButton : null}
              </Upload>
            )}
          </Mutation>
        </div>
      </div>

      <div className="form_cont">
        <Form
          form={form}
          onFinish={(value) => updateDate(value)}
          initialValues={user}
        >
          <Form.Item name="name">
            <Input placeholder="Nombre" className="input_style" />
          </Form.Item>
          <Form.Item name="lastName">
            <Input placeholder="Apellidos" className="input_style" />
          </Form.Item>
          <Form.Item name="email">
            <Input
              placeholder="Correo electrónico"
              className="input_style"
              type="email"
            />
          </Form.Item>
          <Form.Item name="city">
            <Input placeholder="Ciudad" className="input_style" />
          </Form.Item>
          <Form.Item name="telefono">
            <Input placeholder="Teléfono" className="input_style" type="tel" />
          </Form.Item>
          <Form.Item>
            <button type="submit" className="primary_btn">
              Guardar cambios
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
