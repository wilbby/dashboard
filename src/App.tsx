import React from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import PivateRouter from "./PrivateRouter";
import PublicRouter from "./PublicRouter";

import * as Sentry from "@sentry/browser";
import Session from "./Sesion";

////Screen ////
import Login from "./Page/Login";
import Home from "./Page/Home";
import NoFound from "./Page/NoFound";
////End ///

import timezone from "moment-timezone";

timezone().tz("Europe/Berlin").format();

Sentry.init({
  dsn: "https://f955be9c777e4aecb34bb34a01e90f36@sentry.io/1778988",
});

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <PublicRouter exact path="/" component={Login} />
        <PivateRouter exact path="/home" component={Home} />
        <Route component={NoFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default Session(App);
