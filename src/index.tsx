import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import "./index.css";
import "./antd.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { LOCAL_API_URL, LOCAL_API_PATH } from "./Config";
import moment from "moment";

moment().utcOffset(60);

// Configuración del Apollo Client
const client = new ApolloClient({
  uri: LOCAL_API_URL + LOCAL_API_PATH,
  // enviar token al servidor
  fetchOptions: {
    credentials: "include",
  },
  //@ts-ignore
  request: (operation) => {
    const token = localStorage.getItem("token");
    operation.setContext({
      headers: {
        authorization: JSON.stringify(token),
      },
    });
  },
  cache: new InMemoryCache({
    addTypename: false,
  }),
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    // if(graphQLErrors && graphQLErrors[0].extensions.code === 'UNAUTHENTICATED'){
    //   message.error('Debe iniciar sesión para agregar un servicio.');
    // }
    console.log("networkError", networkError);
  },
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
